<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('AUTOSAVE_INTERVAL', 86400);


// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress 
define('DB_NAME', 'sln3w5pa_usa'); */

define('DB_NAME', 'independent_db_live');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+dovp6eGQ2g+%k&a.)]!ya}S(+9V`31lBfDj$WOZ=j*xQ$wKr~q_eA8q$-QF1ImN');
define('SECURE_AUTH_KEY',  'L{gj^P!fGM,Gx)7#O(}$lm0fHJmpy@-[V+A0OshFR;}cY3!${2+-TVKDNfLB)2a-');
define('LOGGED_IN_KEY',    'j-/Kqi)|3ph2|!zd=2xVxLhc~] WSh@gjAgoQwa.T:d4,R>[Ldn@/Vx.vC)@+OGn');
define('NONCE_KEY',        'nfU4M,v}snCSW%->]C{2(njn:<k=A$3r}~Jv]md lDbTwXx-yjycfCW-KKDGtr;L');
define('AUTH_SALT',        'X]EX-|g.M+;qB>Q:=xHTd4-k63w-0WN@%?9Cx$Is}-<_gK?-|?M7:FL-AcJ;Da,V');
define('SECURE_AUTH_SALT', 'EqP~3(!ynBL#55djT@BS&1>I#Xw)/gFX)L10xy%CWD,N.29,SUb<*B?`hgzgMxug');
define('LOGGED_IN_SALT',   '2)/4Rdxp%Oh[22<F<NA4~f! L-.{ml7n[<yNZpU(V7U]]3Fa~|aTO+Gl#4f]H6XD');
define('NONCE_SALT',       '^M?E28`> 4-L^Kn%+rn[`~.X=}]((ru/pJ|w!F=cG?~Hi&!.,M511i*PM$L4JSu3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

error_reporting(0);

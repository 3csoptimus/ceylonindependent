License: GNU General Public License v3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html
SportsMag WordPress Theme, Copyright 2015 AccessPress Themes
SportsMag is distributed under the terms of the GNU GPL v3


Install Steps:
--------------

1. Install parent theme name : AccessPress Mag
2. Install and active theme
3. Go to the Theme Option page
4. Setup theme options
5. Some settings from customizer

-------------------------------------------
Fonts
    Font Awesome: MIT and GPL licenses
    http://fontawesome.io/license/
    
    Oswald
    https://www.google.com/fonts/specimen/Oswald
    SIL Open Font License, 1.1
    
    Open Sans
    http://www.google.com/fonts/specimen/Open+Sans
    Apache License, version 2.0
    
    Dosis
    http://www.google.com/fonts/specimen/Dosis
    SIL Open Font License, 1.1
    
----------------------------------------------------

Images

    https://pixabay.com/en/cycling-competition-race-athletes-813890/
    http://www.pexels.com/photo/sea-man-person-surfer-2340/
    https://pixabay.com/en/soccer-ball-football-players-game-263716/
    https://pixabay.com/en/golf-south-korea-women-s-open-819522/
    https://pixabay.com/en/basketball-female-sport-ball-woman-673581/
    
 
== Changelog ==
Versuib 1.0.4
 * Added responsive code
 * Hide header advertisment section
 
Version 1.0.3
 * Fixed issue listed by reviewer
 * Fixed design bugs
 
Version 1.0.2
 * Fixed listed issue
 * Added div for current date

Version 1.0.1
 * Fixed all error listed by reviewer

Version 1.0.0
* Submitted theme for review in http://wordpress.org
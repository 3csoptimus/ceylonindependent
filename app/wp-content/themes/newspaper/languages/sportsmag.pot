# Loco Gettext template
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: SportsMag\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: Fri Jul 31 2015 15:15:12 GMT+0545 (Nepal Standard Time)\n"
"POT-Revision-Date: Thu Sep 17 2015 12:58:53 GMT+0545 (Nepal Standard Time)\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;"
"__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;"
"_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;"
"esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;"
"esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2\n"
"X-Generator: Loco - https://localise.biz/"

#: ../content-archive-default.php:48 ../content-archive-style1.php:49 ../content-
#: single.php:49 ../content-style1.php:49
msgid "Pages:"
msgstr ""

#: ../functions.php:32
msgid "Child Theme Settings"
msgstr ""

#: ../functions.php:36
msgid "Home Page Settings"
msgstr ""

#: ../functions.php:41 ../functions.php:54
msgid "Popular Articles"
msgstr ""

#: ../functions.php:42
msgid "Show or hide popular ariticle section"
msgstr ""

#: ../functions.php:44 ../functions.php:60
msgid "Show"
msgstr ""

#: ../functions.php:45 ../functions.php:61
msgid "Hide"
msgstr ""

#: ../functions.php:50
msgid "Popular Articles Title"
msgstr ""

#: ../functions.php:51
msgid "Add block name as you like (example: Popular Articles)"
msgstr ""

#: ../functions.php:57
msgid "Youtube Lists"
msgstr ""

#: ../functions.php:58
msgid "Show or hide youtube playlist section"
msgstr ""

#: ../functions.php:66
msgid "Youtube Lists Title"
msgstr ""

#: ../functions.php:67
msgid "Add your title for youtube list section"
msgstr ""

#: ../functions.php:73
msgid "Youtube Video Ids"
msgstr ""

#: ../functions.php:74
msgid ""
"Add youtube id's separated by comma (ex: xrt27dZ7DOA, u8--jALkijM, "
"HusniLw9i68):"
msgstr ""

#: ../functions.php:209
msgid "Youtube Videos"
msgstr ""

#: ../functions.php:332
#, php-format
msgctxt "post date"
msgid "%s"
msgstr ""

#: ../functions.php:343
#, php-format
msgctxt "post author"
msgid "%s"
msgstr ""

#: ../header.php:23
msgid "Skip to content"
msgstr ""

#: ../header.php:55
msgid "Top Menu"
msgstr ""

#: ../header.php:61
msgid "Top Menu Right"
msgstr ""

#: ../home-page.php:28
msgid "Popular text"
msgstr ""

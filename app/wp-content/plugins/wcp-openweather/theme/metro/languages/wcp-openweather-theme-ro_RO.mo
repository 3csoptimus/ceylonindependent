��    !      $  /   ,      �     �               /     E     L  "   R     u     y     �     �     �     �  
   �     �     �     �     �     �     �     
       
   #     .     =     J     f     r     �     �     �  i   �  6    $   ?     d     �     �     �     �  "   �     �     �     �     �  	               
   #     .     L     R     [     p     �     �     �     �     �     �     �     �     �     	     	  f   	                                                     
                                                                                                   !   	    Adds mini weather to sidebar Adds weather to sidebar Background Color Background Opacity, % Clouds Cond. Crop thumbnail to exact dimensions Day Delete Image Global settings Height Humidity Image Image Size Metro Theme Ooops! Nothing was found! Pres. Pressure Reset to Default Save Changes Shortcode settings Temp. Text Color Theme Settings Upload Image Use default plugin settings WCP Weather WCP Weather Mini Widget settings Width Wind You can change current theme <a href="/wp-admin/admin.php?page=wcp-weather&tab=plugin-settings">here</a>. Project-Id-Version: wcp-openweather-theme
POT-Creation-Date: 2015-10-12 16:29+0300
PO-Revision-Date: 2015-10-12 16:32+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: ro_RO
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.5
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Adaugă vreme mini la bara laterală Adaugă vreme la bara laterală Culoare background Opacitate background, % Innorat Cond. Taie imaginea la dimensiuni exacte Zi Sterge imagine Latime Inaltime Umiditate Imagine Marime imagine Tema Metro Hopa! Nimic nu a fost găsit! Pres. Presiune Reseteaza la default Salveaza setari Setari shortcode Temp. Culoare text Setari tema Incarca imagine Foloseste setarile default WCP Weather WCP Weather Mini Setari widget Latime Vant Poti schimba tema curenta <a href="/wp-admin/admin.php?page=wcp-weather&tab=plugin-settings">aici</a>. 
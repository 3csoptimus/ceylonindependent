��          �      L      �     �     �     �                         (     B     H     Q     b     o  
   u     �     �     �  i   �      2     )   F     p     w     �     �     �  #   �     �     �  5   �     2  	   R     \     p     |  
   �  �   �           
                                                                    	                     Adds mini weather to sidebar Adds weather to sidebar Background Color Cond. Day Default Theme Humidity Ooops! Nothing was found! Pres. Pressure Reset to Default Save Changes Temp. Text Color WCP Weather WCP Weather Mini Wind You can change current theme <a href="/wp-admin/admin.php?page=wcp-weather&tab=plugin-settings">here</a>. Project-Id-Version: wcp-openweather-theme
POT-Creation-Date: 2015-08-19 17:05+0300
PO-Revision-Date: 2015-11-12 11:08+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: bg_BG
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Добави мини времето в sidebar-а Добави времето в sidebar-а Фон Условия Ден Основна тема Влажност Упс! Не открих нищо! Налягане Налягане Връщане на основни настройки Запази промените Темп. Цвят текст WCP Weather WCP Weather Мини Вятър Можете да смените темата от <a href="/wp-admin/admin.php?page=wcp-weather&tab=plugin-settings">тук</a>. 
��          �      L      �     �     �     �                         (     B     H     Q     b     o  
   u     �     �     �  i   �  :    6   G  %   ~     �     �     �     �  	   �     �     �     �     �               %     4     @     Q  l   W           
                                                                    	                     Adds mini weather to sidebar Adds weather to sidebar Background Color Cond. Day Default Theme Humidity Ooops! Nothing was found! Pres. Pressure Reset to Default Save Changes Temp. Text Color WCP Weather WCP Weather Mini Wind You can change current theme <a href="/wp-admin/admin.php?page=wcp-weather&tab=plugin-settings">here</a>. Project-Id-Version: wcp-openweather-theme
POT-Creation-Date: 2015-10-11 11:29+0300
PO-Revision-Date: 2015-10-11 11:57+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: sl_SI
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.5
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Dodaj majhno vremensko ikono v stransko orodno vrstico Dodaj vreme v stransko orodno vrstico Barva ozadja Pog. Dan Privzeta tema Vlažnost Ups! Nič nismo našli! Prit. Pritisk Ponastavi na privzeto Shrani spremembe Temp. Barva besedila WCP Weather WCP Weather Mini Veter Trenutno temo lahko spremenite <a href="/wp-admin/admin.php?page=wcp-weather&tab=plugin-settings">tukaj</a>. 
��          �      L      �     �     �     �                         (     B     H     Q     b     o  
   u     �     �     �  i   �      (     #   <     `     q     w     |  
   �     �     �     �  #   �     �     �     �               ,  w   0           
                                                                    	                     Adds mini weather to sidebar Adds weather to sidebar Background Color Cond. Day Default Theme Humidity Ooops! Nothing was found! Pres. Pressure Reset to Default Save Changes Temp. Text Color WCP Weather WCP Weather Mini Wind You can change current theme <a href="/wp-admin/admin.php?page=wcp-weather&tab=plugin-settings">here</a>. Project-Id-Version: wcp-openweather-theme
POT-Creation-Date: 2015-08-19 17:49+0300
PO-Revision-Date: 2015-08-19 17:49+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: sq_AL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Shtoni mini motin në kornizën anësore Shtoni motin në kornizën anësore Ngjyra e sfondit Kush. Dita Lëkura kryesore Lagështia Upps! Nuk u gjend asnjë! Pres. Presioni Rikthejeni në gjëndjen fillestare Ruajini ndryshimet Temp. Ngjyra e tekstit WCP Weather WCP Weather Mini Era Ju mund ta ndryshoni lëkurën e tanishme <a href="/wp-admin/admin.php?page=wcp-weather&tab=plugin-settings">këtu</a>. 
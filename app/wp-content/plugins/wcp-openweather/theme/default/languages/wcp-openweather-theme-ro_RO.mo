��          �      L      �     �     �     �                         (     B     H     Q     b     o  
   u     �     �     �  i   �  6    $   C     h     �     �     �     �  	   �     �     �     �     �     �                     ,     =  f   B           
                                                                    	                     Adds mini weather to sidebar Adds weather to sidebar Background Color Cond. Day Default Theme Humidity Ooops! Nothing was found! Pres. Pressure Reset to Default Save Changes Temp. Text Color WCP Weather WCP Weather Mini Wind You can change current theme <a href="/wp-admin/admin.php?page=wcp-weather&tab=plugin-settings">here</a>. Project-Id-Version: wcp-openweather-theme
POT-Creation-Date: 2015-10-12 15:52+0300
PO-Revision-Date: 2015-10-12 16:28+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: ro_RO
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.5
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Adaugă vreme mini la bara laterală Adaugă vreme la bara laterală Culoare background Cond. Zi Tema default Umiditate Hopa! Nimic nu a fost găsit! Pres. Presiune Reseteaza la default Salveaza setari Temp. Culoare text WCP Weather WCP Weather Mini Vant Poti schimba tema curenta <a href="/wp-admin/admin.php?page=wcp-weather&tab=plugin-settings">aici</a>. 
��          �      L      �     �     �     �                         (     B     H     Q     b     o  
   u     �     �     �  i   �  P    P   ]  4   �     �                    -  .   >  	   m     w     �  #   �  	   �     �     �     �  
     �              
                                                                    	                     Adds mini weather to sidebar Adds weather to sidebar Background Color Cond. Day Default Theme Humidity Ooops! Nothing was found! Pres. Pressure Reset to Default Save Changes Temp. Text Color WCP Weather WCP Weather Mini Wind You can change current theme <a href="/wp-admin/admin.php?page=wcp-weather&tab=plugin-settings">here</a>. Project-Id-Version: wcp-openweather-theme
POT-Creation-Date: 2015-08-19 17:52+0300
PO-Revision-Date: 2015-08-19 17:52+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: sr_RS
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Додај малу временску прогнозу у бочну траку Додај прогнозу у бочну траку Позадинска боја Услови Дан Основна тема Влажност Упс! Ништа није пронађено Прит. Притисак Врати на основна Сачувај подешавања Темп. Боја текста WCP Weather WCP Weather Mini Ветар Можете променити тренутну тему <a href="/wp-admin/admin.php?page=wcp-weather&tab=plugin-settings">овде</a>. 
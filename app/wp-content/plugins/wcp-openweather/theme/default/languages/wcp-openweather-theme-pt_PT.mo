��          �      L      �     �     �     �                         (     B     H     Q     b     o  
   u     �     �     �  i   �      $        8     X     e     k     o     �     �     �     �     �     �     �     �     �            k              
                                                                    	                     Adds mini weather to sidebar Adds weather to sidebar Background Color Cond. Day Default Theme Humidity Ooops! Nothing was found! Pres. Pressure Reset to Default Save Changes Temp. Text Color WCP Weather WCP Weather Mini Wind You can change current theme <a href="/wp-admin/admin.php?page=wcp-weather&tab=plugin-settings">here</a>. Project-Id-Version: wcp-openweather-theme
POT-Creation-Date: 2015-09-28 17:44+0300
PO-Revision-Date: 2015-09-28 17:44+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: pt_PT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Adiciona mini clima à barra lateral Adiciona clima à barra lateral Cor de fundo Cond. Dia Tema por omissão Humidade Ups! Nada foi encontrado! Pres. Pressão Restabelecer para Omissão Guardar Alterações Temp. Cor de texto WCP Weather WCP Weather Mini Vento Pode alterar o seu tema actual <a href="/wp-admin/admin.php?page=wcp-weather&tab=plugin-settings">aqui</a>. 
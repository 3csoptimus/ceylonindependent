��    o      �  �         `	     a	     s	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  !   �	     �	     �	  	   
     
     
     "
     2
     4
     8
     <
     U
     i
     }
     �
  *   �
  *   �
  �   �
     �     �     �  %  �     �     �     �     �     �  "   �     �     �                
                1     3     6     :     >     A     P     ]     d      m     �     �     �     �     �     �     �     �     �                    )     -     1  
   9  �   D     �     �     �     �     �     �     �     �     �                                   $     )     -     1     5     :     ?     C     G     K     O     S     W     [     _     c     g     l     p  c  t     �     �     �     �               #     '     *     -     0     3     7     E  #   J  	   n     x     �     �     �     �     �     �     �  !   �  *   �  *   $  .   O     ~  -   �  %   �  �   �     �     �     �  F  �     �     �     �     �     �     �     	                    '     0     ?     V     X     [     _     c     f     v     �     �  -   �     �     �  
   	               (     1     :     G     ]     _     ~     �     �     �     �  �   �     R     X     \     e     i     q     x     }     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �                                     #          S           	       K      %   6   ,   J   7       a   O       M   b      
            [              (      .         1       W   ?           \       j   R   h   N   E   !       8       4          i      U   n   Y         ]   <   _                 B      `       +   &   ^   -   9   ;   3   Q          @   0               I       X   H          l   m      :      o       e   >   d   g   /   A   L   "      F   D   2   Z       G                 C   '         V   =       $   k               P          c       T       *      )   5   f        "No Data" Message &deg;C &deg;F 0.5h - Not Recommended 12h 1h - Recommended 24h 2h 3h 6h 9h API API key Add Add new WCP OpenWeather shortcode Always Auto Detect City Name Current Theme Default Display Options E ENE ESE Enable "Google Maps API" Enable User Options Enable user options Expire User Options (days) General Hide Description of the Weather Conditions Hide description of the weather conditions How to get API key you can see <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">here</a>. Knots Language Location More information about the plugin you can find on the <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">plugin page</a> in the <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> and <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a> sections. <strong>Live demo</strong> you can find on <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">our site</a>. N NE NNE NNW NW Not supported in the current theme Options Other Pa Plugin Pressure Refresh Time Reset to Default S SE SSE SSW SW Save & Refresh Save Changes Select Settings Settings reset to default values Show 5 day forecast Show current weather Temperature Template Theme Settings Title Units Upload Image User Options W WCP OpenWeather Shortcode WCP Weather WNW WSW Weather Wind Speed You can find you city name on <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. apr atm aug bar dec feb fri hPa inHg jan jul jun kPa kgf/cm² kgf/m² km/h m/s mar may mbar mmHg mon mph nov oct psf psi sat sep sun thu torr tue wed Project-Id-Version: wcp-openweather
POT-Creation-Date: 2015-11-19 12:48+0300
PO-Revision-Date: 2015-11-19 13:06+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: tr_TR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: assets
X-Poedit-SearchPath-1: classes
X-Poedit-SearchPath-2: config
X-Poedit-SearchPath-3: templates
 "Veri yok" iletisi &deg;C &deg;F 0.5s - Önerilmez 12s 1s - Tavsiye edilir 24s 2s 3s 6s 9s API API Anahtarı Ekle Yeni WCP OpenWeather kısa kod ekle Her zaman Otomatik Algılama Şehir İsmi Güncel Tema Varsayılan Görüntüleme Seçenekleri D DKD DGD "Google Maps API" etkinleştirmek Kullanıcı seçeneklerini etkinleştirmek Kullanıcı seçeneklerini etkinleştirmek Kullanıcı Seçenekleri Bitiş Süresi (gün) Genel Hava koşullarının açıklamalarını gizle Hava koşullarının açıklama gizle API anahtarının nasıl alındığını görmek için <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">tıklayın</a>. Knots Dil Lokasyon Daha fazla bilgi için <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">eklenti sayfası</a>, sorularınız için <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">SSS Sayfası</a> ve <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Ekran Görüntüleri</a>'ni bulabilirsiniz. <strong>Demo Gösterimi</strong> sayfamızda bulabilirsiniz.<a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">Tıklayın</a>. K KD KKD KKB KB Geçerli teman desteklenmiyor Seçenekler Diğer Pa Eklenti Basınç Yenile Zamanı Varsayılana Geri Dön G GD GGD GGB GB Kaydet & Yenile Değişiklikleri Kaydet Seçiniz Ayarlar Ayarlarını varsayılan değerlere sıfırla 5 günlük hava tahmini göster Güncel hava durumu göster Sıcaklık Tema Tema Ayarları Başlık Birimler Resim Yükle Kullanıcı Ayarları B Yeni WCP OpenWeather kısa kod WCP Weather BKB BGB Hava Durumu Rüzgar Hızı Şehir isimlerini buradan bulabilirsiniz <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. nisan atm ağustos bar aralık şubat cuma ﻿hPa inHg ocak temmuz haziran kPa kgf/cm² kgf/m² km/h m/s mart mayıs mbar mmHg ptesi mph kasım ekim psf psi ctesi eylül pazar perş torr salı çar 
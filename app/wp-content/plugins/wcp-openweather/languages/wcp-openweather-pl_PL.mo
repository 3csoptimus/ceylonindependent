��    o      �  �         `	     a	     s	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  !   �	     �	     �	  	   
     
     
     "
     2
     4
     8
     <
     U
     i
     }
     �
  *   �
  *   �
  �   �
     �     �     �  %  �     �     �     �     �     �  "   �     �     �                
                1     3     6     :     >     A     P     ]     d      m     �     �     �     �     �     �     �     �     �                    )     -     1  
   9  �   D     �     �     �     �     �     �     �     �     �                                   $     )     -     1     5     :     ?     C     G     K     O     S     W     [     _     c     g     l     p  �  t  #        6     =     D     W     [     l     p     s     v     y     |  	   �     �  $   �     �     �     �     �  
   �     �                         3     N  "   i     �  %   �  &   �  �   �     v     ~     �    �     �     �     �     �     �  '   �     �     �     �     �  
   �     
          1     4     8     >     D     H     _     m  
   u  9   �     �     �     �     �                     '     6     I     K     e     q     v     {     �  �   �     4     8     <     @     D     I     M     P     T     Y     ]     a     e     i     r     z          �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �                 #          S           	       K      %   6   ,   J   7       a   O       M   b      
            [              (      .         1       W   ?           \       j   R   h   N   E   !       8       4          i      U   n   Y         ]   <   _                 B      `       +   &   ^   -   9   ;   3   Q          @   0               I       X   H          l   m      :      o       e   >   d   g   /   A   L   "      F   D   2   Z       G                 C   '         V   =       $   k               P          c       T       *      )   5   f        "No Data" Message &deg;C &deg;F 0.5h - Not Recommended 12h 1h - Recommended 24h 2h 3h 6h 9h API API key Add Add new WCP OpenWeather shortcode Always Auto Detect City Name Current Theme Default Display Options E ENE ESE Enable "Google Maps API" Enable User Options Enable user options Expire User Options (days) General Hide Description of the Weather Conditions Hide description of the weather conditions How to get API key you can see <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">here</a>. Knots Language Location More information about the plugin you can find on the <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">plugin page</a> in the <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> and <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a> sections. <strong>Live demo</strong> you can find on <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">our site</a>. N NE NNE NNW NW Not supported in the current theme Options Other Pa Plugin Pressure Refresh Time Reset to Default S SE SSE SSW SW Save & Refresh Save Changes Select Settings Settings reset to default values Show 5 day forecast Show current weather Temperature Template Theme Settings Title Units Upload Image User Options W WCP OpenWeather Shortcode WCP Weather WNW WSW Weather Wind Speed You can find you city name on <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. apr atm aug bar dec feb fri hPa inHg jan jul jun kPa kgf/cm² kgf/m² km/h m/s mar may mbar mmHg mon mph nov oct psf psi sat sep sun thu torr tue wed Project-Id-Version: wcp-openweather
POT-Creation-Date: 2015-11-19 12:48+0300
PO-Revision-Date: 2015-11-27 15:48+0200
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.6
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: assets
X-Poedit-SearchPath-1: classes
X-Poedit-SearchPath-2: config
X-Poedit-SearchPath-3: templates
 Pojawi się komunikat "Brak danych" &deg;C &deg;F 0.5h - Niezalecane 12h 1h - Niepolecane 24h 2h 3h 6h 9h API Klucz API Dodaj Dodaj nowy WCP OpenWeather shortcode Zawsze Automatyczne wykrywanie Nazwa miasta Aktywny Motyw Domyślnie Opcje wyświetlania W WPW WPdW Pozwól "Google Maps API" Pozwól opcji użytkownika Pozwól opcji użytkownika Wygasają opcje użytkownika (dni) Ogólne Nie pokazuj opis pogodowych warunków Nie pokazuj opis pogodowych warunków  Jak zdobyć klucz API możesz zobaczyć <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">tutaj</a>. Węzły Język Lokalizacja Więcej informacji na temat wtyczki można znaleźć na <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">stronie wtyczki</a> w <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> i <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Zdjęcia">Zdjęcia</a> sekcjach. Live demo można znaleźć na <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">naszej stronie</a>. Pn PnW PnPnW PnPnZ PnZ Nie jest obsługiwany w aktualnym motyw Opcje Inne  Pa Wtyczka Ciśnienie Czas aktualizacji Przywróć Domyślne Pd PdW PdPdW PdPdZ PdZ Zapisz i Aktualizować Zapisz Zmiany Wybierz Ustawienia Ustawienia zostały przywrócone do wartości domyślnych Pokaż prognoza 5-dniowa Pokaż aktualną pogodę Temperatura Szablon Ustawienia Motyw Tytuł Jednostki miar Załaduj obraz Opcje użytkownika Z WCP OpenWeather Shortcode WCP Weather ZPnZ ZPdZ Pogoda Prędkość wiatru Możesz znaleźć nazwę miasta na <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. kwi atm sie bar  gru lut pi hPa inHg sty lip cze kPa kgf/cm² kgf/m² km/h m/s mar maj mbar mmHg po mph lis paź psf psi so wrz ni cz torr wt śr 
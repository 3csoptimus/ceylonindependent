��    o      �  �         `	     a	     s	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  !   �	     �	     �	  	   
     
     
     "
     2
     4
     8
     <
     U
     i
     }
     �
  *   �
  *   �
  �   �
     �     �     �  %  �     �     �     �     �     �  "   �     �     �                
                1     3     6     :     >     A     P     ]     d      m     �     �     �     �     �     �     �     �     �                    )     -     1  
   9  �   D     �     �     �     �     �     �     �     �     �                                   $     )     -     1     5     :     ?     C     G     K     O     S     W     [     _     c     g     l     p  �  t          3     :     A     ]     e          �     �     �     �     �  
   �     �  0   �  	   �     �                0     I     ^     c     h     l     �     �  %   �  
   �  $   �  $     �   >     �     �     �  )  �                              "  
   B     M     R  	   U  	   _     i     |     �     �     �     �     �     �     �     �  
   �  *   �          3     K  	   Y     c     t     �     �     �     �      �     �     �     �     �     �  �   �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �                                         "     &     *     .     3     7                 #          S           	       K      %   6   ,   J   7       a   O       M   b      
            [              (      .         1       W   ?           \       j   R   h   N   E   !       8       4          i      U   n   Y         ]   <   _                 B      `       +   &   ^   -   9   ;   3   Q          @   0               I       X   H          l   m      :      o       e   >   d   g   /   A   L   "      F   D   2   Z       G                 C   '         V   =       $   k               P          c       T       *      )   5   f        "No Data" Message &deg;C &deg;F 0.5h - Not Recommended 12h 1h - Recommended 24h 2h 3h 6h 9h API API key Add Add new WCP OpenWeather shortcode Always Auto Detect City Name Current Theme Default Display Options E ENE ESE Enable "Google Maps API" Enable User Options Enable user options Expire User Options (days) General Hide Description of the Weather Conditions Hide description of the weather conditions How to get API key you can see <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">here</a>. Knots Language Location More information about the plugin you can find on the <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">plugin page</a> in the <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> and <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a> sections. <strong>Live demo</strong> you can find on <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">our site</a>. N NE NNE NNW NW Not supported in the current theme Options Other Pa Plugin Pressure Refresh Time Reset to Default S SE SSE SSW SW Save & Refresh Save Changes Select Settings Settings reset to default values Show 5 day forecast Show current weather Temperature Template Theme Settings Title Units Upload Image User Options W WCP OpenWeather Shortcode WCP Weather WNW WSW Weather Wind Speed You can find you city name on <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. apr atm aug bar dec feb fri hPa inHg jan jul jun kPa kgf/cm² kgf/m² km/h m/s mar may mbar mmHg mon mph nov oct psf psi sat sep sun thu torr tue wed Project-Id-Version: wcp-openweather
POT-Creation-Date: 2015-11-19 12:48+0300
PO-Revision-Date: 2015-11-19 13:00+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: lt_LT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: assets
X-Poedit-SearchPath-1: classes
X-Poedit-SearchPath-2: config
X-Poedit-SearchPath-3: templates
 "Negauta duomenų" žinutė &deg;C &deg;F 0.5 val. - Nerekomenduojama 12 val. 1 val. - Nerekomenduojama 24 val. 2 val. 3 val. 6 val. 9 val. API API raktas Pridėti Pridėti naują WCP OpenWeather trumpąjį kodą Pastoviai Automatinis nustatymas Miesto pavadinimas Dabartinė tema Standartiniai nustatymai Pateikimo nustatymai RŠR RŠR RPR Įjungti "Google Maps API" Įjungti vartotojo nustatymus Įjungti vartotojo nustatymus Vartotojo nustatymų galiojimo laikas Bendriniai Paslėpti oro sąlygų apibūdinimus Paslėpti oro sąlygų apibūdinimus Kaip gauti API raktą galite rasti  <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">čia</a>. mazgai Kalba Vieta Daugiau informacijos apie įskiepį galite rasti jo <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">puslapyje</a> <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> ir <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a> sekcijose, veikiančią demonstracinę versiją galite rasti <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">mūsų puslapyje</a>. Š ŠR ŠŠR ŠŠV ŠV Nepalaikoma dabartinėje temoje Nustatymai Kiti Pa Įskiepis Spaudimas Atnaujinimo laikas Grąžinti pirminius nustatymus P PR PPR PPV PV Išsaugoti ir atnaujinti Išsaugoti pakeitimus Rinktis Nustatymai Nustatymų grąžinimas į pirmines vertes Rodyti 5 dienų prognozę Rodyti dabartinius orus Temperatūra  Šablonas Temos nustatymai Pavadinimas Vienetai Įkelti nuotrauką Vartotojo nustatymai V WCP OpenWheather trumpasis kodas WCP Weather VŠV VPV Orai Vėjo greitis Savo miesto pavadinimą galite rasti <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. bal atm rug bar gru vas pen hPa inHg sau lie bir kPa kgf/cm² kgf/m² km/val m/s kov geg mbar mmHg pir myl/val lap spa psf psi šeš rug sek ket torr ant tre 
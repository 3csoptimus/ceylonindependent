��    o      �  �         `	     a	     s	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  !   �	     �	     �	  	   
     
     
     "
     2
     4
     8
     <
     U
     i
     }
     �
  *   �
  *   �
  �   �
     �     �     �  %  �     �     �     �     �     �  "   �     �     �                
                1     3     6     :     >     A     P     ]     d      m     �     �     �     �     �     �     �     �     �                    )     -     1  
   9  �   D     �     �     �     �     �     �     �     �     �                                   $     )     -     1     5     :     ?     C     G     K     O     S     W     [     _     c     g     l     p  c  t     �     �     �     �               /     3     6     9     <     ?  	   C  	   M  ,   W     �     �     �     �     �     �     �     �     �     �      �         -   A     o  4   u  4   �  �   �     r     w       ,  �     �     �     �     �     �      �     �     �     �     �               &     A     C     F     J     N     Q     e     z     �  ,   �     �     �     �     �                    1     A     X     Z     t     �     �     �     �  �   �     E     I     M     Q     U     Y     ]     a     e     j     n     r     v     z     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �                 #          S           	       K      %   6   ,   J   7       a   O       M   b      
            [              (      .         1       W   ?           \       j   R   h   N   E   !       8       4          i      U   n   Y         ]   <   _                 B      `       +   &   ^   -   9   ;   3   Q          @   0               I       X   H          l   m      :      o       e   >   d   g   /   A   L   "      F   D   2   Z       G                 C   '         V   =       $   k               P          c       T       *      )   5   f        "No Data" Message &deg;C &deg;F 0.5h - Not Recommended 12h 1h - Recommended 24h 2h 3h 6h 9h API API key Add Add new WCP OpenWeather shortcode Always Auto Detect City Name Current Theme Default Display Options E ENE ESE Enable "Google Maps API" Enable User Options Enable user options Expire User Options (days) General Hide Description of the Weather Conditions Hide description of the weather conditions How to get API key you can see <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">here</a>. Knots Language Location More information about the plugin you can find on the <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">plugin page</a> in the <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> and <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a> sections. <strong>Live demo</strong> you can find on <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">our site</a>. N NE NNE NNW NW Not supported in the current theme Options Other Pa Plugin Pressure Refresh Time Reset to Default S SE SSE SSW SW Save & Refresh Save Changes Select Settings Settings reset to default values Show 5 day forecast Show current weather Temperature Template Theme Settings Title Units Upload Image User Options W WCP OpenWeather Shortcode WCP Weather WNW WSW Weather Wind Speed You can find you city name on <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. apr atm aug bar dec feb fri hPa inHg jan jul jun kPa kgf/cm² kgf/m² km/h m/s mar may mbar mmHg mon mph nov oct psf psi sat sep sun thu torr tue wed Project-Id-Version: wcp-openweather
POT-Creation-Date: 2015-11-19 12:48+0300
PO-Revision-Date: 2015-11-19 13:03+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: pt_PT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: assets
X-Poedit-SearchPath-1: classes
X-Poedit-SearchPath-2: config
X-Poedit-SearchPath-3: templates
 Mensagem "Sem dados" &deg;C &deg;F 0.5h -  - Não Recomendável 12h 1h - Recomendável 24h 2h 3h 6h 9h API Chave API Adicionar Adicionar um novo WCP OpenWeather shortcode  Sempre Detectar Automaticamente Nome da cidade Tema actual Omissão Mostrar opções E ENE ESE Autorizar "Google Maps API" Autorizar opções de utilizador Autorizar opções de utilizador Expiração das opções de utilizador (dias) Geral Esconder a descrição das condições climatéricas Esconder a descrição das condições climatéricas Como obter a chave API, pode ser vista <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">aqui</a>. nós Língua Localização Para mais informações referentes ao plugin, visitar a <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">página do plugin</a> nas secções <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> e <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a>. O demo em tempo real por ser encontrado no <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">nosso site</a>.  N NE NNE NNW NW Não é suportado no tema actual Opções Outros Pa Plugin Pressão Tempo de actualização Restabelecer para Omissão S SE SSE SSW SW Salvar e Actualizar Guardar Alterações Seleccionar Opções Restaurar opções aos valores por omissão  Mostrar 5 dias de previsão  Mostrar o clima actual Temperatura Template Opções do Tema Título Unidades de medida Carregar imagem Opções de utilizador W WCP OpenWeather Shortcode WCP Weather WNW WSW Clima Velocidade do vento Pode encontrar o nome da sua cidade em <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. abr atm aug bar dec fev sex hPa inHg jan jul jun kPa kgf/cm² kgf/m² km/h m/s mar maio mbar mmHg seg mph nov out psf psi sáb set dom qui torr ter qua 
��    o      �  �         `	     a	     s	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  !   �	     �	     �	  	   
     
     
     "
     2
     4
     8
     <
     U
     i
     }
     �
  *   �
  *   �
  �   �
     �     �     �  %  �     �     �     �     �     �  "   �     �     �                
                1     3     6     :     >     A     P     ]     d      m     �     �     �     �     �     �     �     �     �                    )     -     1  
   9  �   D     �     �     �     �     �     �     �     �     �                                   $     )     -     1     5     :     ?     C     G     K     O     S     W     [     _     c     g     l     p  c  t     �     �     �               '     A     I     P     W     ^     e     i     v  8     
   �     �     �     �     �     �                      &   2  &   Y  0   �     �  )   �  )   �  �        �     �  
   �  G  �                         !  $   $  
   I  	   T     ^     a     h     q  #   �     �     �     �     �     �     �     �  
   �  
   �  0   �  )   )  *   S     ~     �     �     �  	   �     �     �     �  #   �                         !  �   5     �     �     �     �     �     �     �     �     �     �     �                                    "     &     *     /     4     9     =     B     F     J     N     R     V     Z     ^     c     g                 #          S           	       K      %   6   ,   J   7       a   O       M   b      
            [              (      .         1       W   ?           \       j   R   h   N   E   !       8       4          i      U   n   Y         ]   <   _                 B      `       +   &   ^   -   9   ;   3   Q          @   0               I       X   H          l   m      :      o       e   >   d   g   /   A   L   "      F   D   2   Z       G                 C   '         V   =       $   k               P          c       T       *      )   5   f        "No Data" Message &deg;C &deg;F 0.5h - Not Recommended 12h 1h - Recommended 24h 2h 3h 6h 9h API API key Add Add new WCP OpenWeather shortcode Always Auto Detect City Name Current Theme Default Display Options E ENE ESE Enable "Google Maps API" Enable User Options Enable user options Expire User Options (days) General Hide Description of the Weather Conditions Hide description of the weather conditions How to get API key you can see <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">here</a>. Knots Language Location More information about the plugin you can find on the <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">plugin page</a> in the <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> and <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a> sections. <strong>Live demo</strong> you can find on <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">our site</a>. N NE NNE NNW NW Not supported in the current theme Options Other Pa Plugin Pressure Refresh Time Reset to Default S SE SSE SSW SW Save & Refresh Save Changes Select Settings Settings reset to default values Show 5 day forecast Show current weather Temperature Template Theme Settings Title Units Upload Image User Options W WCP OpenWeather Shortcode WCP Weather WNW WSW Weather Wind Speed You can find you city name on <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. apr atm aug bar dec feb fri hPa inHg jan jul jun kPa kgf/cm² kgf/m² km/h m/s mar may mbar mmHg mon mph nov oct psf psi sat sep sun thu torr tue wed Project-Id-Version: wcp-openweather
POT-Creation-Date: 2015-11-19 12:48+0300
PO-Revision-Date: 2015-11-19 13:04+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: sq_AL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: assets
X-Poedit-SearchPath-1: classes
X-Poedit-SearchPath-2: config
X-Poedit-SearchPath-3: templates
 Mesazhi "Asnjë e dhënë" &deg;C &deg;F 0.5 orë - E pakëshillueshme 12 orë 1 orë - E këshillueshme 24 orë 2 orë 3 orë 6 orë 9 orë API Çelësi API Shtojeni Shtoni një kod të ri të shkurtur për WCP OpenWeather Gjithmonë Zbulim automatik Qyteti Lëkura aktuale Mospagim Paraqitni Alternativat E ENE ESE Aktivizoni "Google Maps API" Aktivizoni alternativat e përdoruesit Aktivizoni alternativat e përdoruesit Skadimi i alternativave të përdoruesit (ditë) Të Përgjithshme Fshihni përshkrimin e kushteve të motit Fshihni përshkrimin e kushteve të motit <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">Këtu</a> mund të mësoni sesi të përdorni çelësin API Knots Gjuha Lokalizimi Më shumë informacione mbi këtë plugin mund ta gjeni në <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">faqen e tij</a>, më në detaje në <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">PEP</a> dhe sektorin e <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">fotove prezantuese</a>. Ju mund ta shikoni këtë plugin <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">live në websitin tonë</a>.  N NE NNE NNW NW Nuk suportohet në lëkurën aktuale Veçoritë Të tjera Pa Plugin Presioni Koha rifreskuese Rikthejeni në gjëndjen fillestare S SE SSE SSW SW Ruajeni dhe rifreskojeni Ruajini ndryshimet Përzgjedh Veçoritë Veçoritë për rikthim në gjëndjen fillestare Tregoni parashikimin e motit për 5 ditë Tregoni parashikimin e motit të tanishëm Temperatura Rigë Veçoritë e lëkurës Titulli Njësitë Ngarkoni imazhin Alternativa Përdoruesi W WPC OpenWeather kode të shkurtuara WCP Weather WNW WSW Moti Shpejtësia e erës Ju mund ta gjeni qytetin tuaj në <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. pri atm gus bar dhj shk pre hPa inHg jan kor qer kPa kgf/cm² kgf/m² km/h m/s mar maj mbar mmHg hën mph nën tet psf psi sht sht die enj torr mar mër 
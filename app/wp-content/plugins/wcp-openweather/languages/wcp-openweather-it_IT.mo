��    o      �  �         `	     a	     s	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  !   �	     �	     �	  	   
     
     
     "
     2
     4
     8
     <
     U
     i
     }
     �
  *   �
  *   �
  �   �
     �     �     �  %  �     �     �     �     �     �  "   �     �     �                
                1     3     6     :     >     A     P     ]     d      m     �     �     �     �     �     �     �     �     �                    )     -     1  
   9  �   D     �     �     �     �     �     �     �     �     �                                   $     )     -     1     5     :     ?     C     G     K     O     S     W     [     _     c     g     l     p  c  t     �     �     �                    /     3     6     9     <     ?  
   C     N  .   W     �     �     �     �     �     �     �     �     �     �                3     T  '   ]  '   �  �   �     5     :  	   A    K     g     i     l     p     t      w     �     �     �     �  	   �     �     �     �     �     �     �     �     �       	   	  
             >     Y     n     z     �     �     �     �     �     �     �     �     �     �     �     �  �        �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �                
                              "     &     *     .     2     7     ;                 #          S           	       K      %   6   ,   J   7       a   O       M   b      
            [              (      .         1       W   ?           \       j   R   h   N   E   !       8       4          i      U   n   Y         ]   <   _                 B      `       +   &   ^   -   9   ;   3   Q          @   0               I       X   H          l   m      :      o       e   >   d   g   /   A   L   "      F   D   2   Z       G                 C   '         V   =       $   k               P          c       T       *      )   5   f        "No Data" Message &deg;C &deg;F 0.5h - Not Recommended 12h 1h - Recommended 24h 2h 3h 6h 9h API API key Add Add new WCP OpenWeather shortcode Always Auto Detect City Name Current Theme Default Display Options E ENE ESE Enable "Google Maps API" Enable User Options Enable user options Expire User Options (days) General Hide Description of the Weather Conditions Hide description of the weather conditions How to get API key you can see <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">here</a>. Knots Language Location More information about the plugin you can find on the <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">plugin page</a> in the <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> and <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a> sections. <strong>Live demo</strong> you can find on <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">our site</a>. N NE NNE NNW NW Not supported in the current theme Options Other Pa Plugin Pressure Refresh Time Reset to Default S SE SSE SSW SW Save & Refresh Save Changes Select Settings Settings reset to default values Show 5 day forecast Show current weather Temperature Template Theme Settings Title Units Upload Image User Options W WCP OpenWeather Shortcode WCP Weather WNW WSW Weather Wind Speed You can find you city name on <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. apr atm aug bar dec feb fri hPa inHg jan jul jun kPa kgf/cm² kgf/m² km/h m/s mar may mbar mmHg mon mph nov oct psf psi sat sep sun thu torr tue wed Project-Id-Version: wcp-openweather
POT-Creation-Date: 2015-11-19 12:48+0300
PO-Revision-Date: 2015-11-19 12:59+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: assets
X-Poedit-SearchPath-1: classes
X-Poedit-SearchPath-2: config
X-Poedit-SearchPath-3: templates
 Messaggio “Nessun dato” &deg;C &deg;F 0.5h - Non Raccomandato 12h 1h - Recommended 24h 2h 3h 6h 9h API Chiave API Aggiungi Aggiungi un nuovo collegamento WCP OpenWeather Sempre Selezione automatica Città Tema corrente Predefinito Opzioni di visualizzazione E ENE ESE Abilità "Google Maps API" Abilità opzioni utente Abilità Opzioni Utente Scadenza opzioni utente (giorni) Generale Nasconti descrizione condizioni attuali Nasconti descrizione condizioni attuali Come ottenere una chiave API <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">qui</a>. Nodi Lingua Località Maggiori informazioni sul plugin le puoi trovare sulla <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">pagina plugin</a> nelle sezioni <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> e <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a>. Puoi trovare una Live Demo sul <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">nostro sito</a>. N NE NNE NNO NO Non supportato nel tema corrente Opzioni  Altro Pa Plugin Pressione Aggiornamento Ripristina a predefinito S SE SSE SSO SO Salva e Aggiorna Salva Seleziona Proprietà Ripristina i valori predefiniti Mostra previsioni 5 grioni Mostra meteo attuale Temperatura Stile Proprietà del Tema Titolo Unità Invia Immagine Opzioni Utente O Collegamento a WCP OpenWeather WCP Weather ONO OSO Meteo Velocità del vento Puoi trovare la tua città su <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. apr atm ago bar dic feb ven hPa inHg gen lug giu kPa kgf/cm² kgf/m² km/h m/s mar mag mbar mmHg lun mph nov ott psf psi sab set dom gio torr mar mer 
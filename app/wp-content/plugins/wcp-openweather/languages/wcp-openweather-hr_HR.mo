��    o      �  �         `	     a	     s	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  !   �	     �	     �	  	   
     
     
     "
     2
     4
     8
     <
     U
     i
     }
     �
  *   �
  *   �
  �   �
     �     �     �  %  �     �     �     �     �     �  "   �     �     �                
                1     3     6     :     >     A     P     ]     d      m     �     �     �     �     �     �     �     �     �                    )     -     1  
   9  �   D     �     �     �     �     �     �     �     �     �                                   $     )     -     1     5     :     ?     C     G     K     O     S     W     [     _     c     g     l     p  �  t     "     9     @     G     c     g     y     }     �     �     �     �  
   �     �  &   �     �     �  	   �     �     �     �     
                    /     M      k  	   �  !   �  !   �  �   �     r     v     |  #  �     �     �     �     �     �     �     �     �     �     �     �     �     
                #     '     +     .     @     P     X  *   a     �     �     �     �     �     �     �     �                    9     E     I     M     U  �   c     �               	                              #     '     +     /     3     <     D     I     M     R     V     [     `     d     l     p     t     x     |     �     �     �     �     �     �                 #          S           	       K      %   6   ,   J   7       a   O       M   b      
            [              (      .         1       W   ?           \       j   R   h   N   E   !       8       4          i      U   n   Y         ]   <   _                 B      `       +   &   ^   -   9   ;   3   Q          @   0               I       X   H          l   m      :      o       e   >   d   g   /   A   L   "      F   D   2   Z       G                 C   '         V   =       $   k               P          c       T       *      )   5   f        "No Data" Message &deg;C &deg;F 0.5h - Not Recommended 12h 1h - Recommended 24h 2h 3h 6h 9h API API key Add Add new WCP OpenWeather shortcode Always Auto Detect City Name Current Theme Default Display Options E ENE ESE Enable "Google Maps API" Enable User Options Enable user options Expire User Options (days) General Hide Description of the Weather Conditions Hide description of the weather conditions How to get API key you can see <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">here</a>. Knots Language Location More information about the plugin you can find on the <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">plugin page</a> in the <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> and <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a> sections. <strong>Live demo</strong> you can find on <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">our site</a>. N NE NNE NNW NW Not supported in the current theme Options Other Pa Plugin Pressure Refresh Time Reset to Default S SE SSE SSW SW Save & Refresh Save Changes Select Settings Settings reset to default values Show 5 day forecast Show current weather Temperature Template Theme Settings Title Units Upload Image User Options W WCP OpenWeather Shortcode WCP Weather WNW WSW Weather Wind Speed You can find you city name on <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. apr atm aug bar dec feb fri hPa inHg jan jul jun kPa kgf/cm² kgf/m² km/h m/s mar may mbar mmHg mon mph nov oct psf psi sat sep sun thu torr tue wed Project-Id-Version: wcp-openweather
POT-Creation-Date: 2015-11-19 12:48+0300
PO-Revision-Date: 2015-11-19 12:59+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: hr_HR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: assets
X-Poedit-SearchPath-1: classes
X-Poedit-SearchPath-2: config
X-Poedit-SearchPath-3: templates
 "Nema podataka" poruka &deg;C &deg;F 30min – Nije Preporučeno 12h 1h - Preporučeno 24h 2h 3h 6h 9h API API ključ Dodaj Dodaj novi WCP OpenWeather kratki kôd Uvijek Auto detekcija Ime grada Trenutna tema Zadano Prikaži opcije I ISI IJI Omogući "Google Maps API" Omogući korisničke postavke Omogući korisničke postavke Istek korisničkih opcija (dana) Generalno Sakrij opis o vremenskim uvjetima Sakrij opis o vremenskim uvjetima Kako nabaviti API ključ možete pogledati <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">ovdje</a>. m/s Jezik Lokacija Više informacija o  <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">dodatku</a> možete pronaći na stranici sa dodatcima <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> i <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Slike zaslona">Slike zaslona</a> odjelu. Uživo demo možete pronaći na <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Uživo Demo">našim stranicama</a>. S SI SSI SSZ SZ Nije podržano u trenutnoj temi Postavke Ostalo Pa Dodatak Tlak Osvježi vrijeme Resetiraj na Zadano J JI JJI JJZ JZ Spremi & Osvježi Spremi Promjene Odaberi Postavke Resetiranje postavki na zadane vrijednosti Prikaži vrijeme u 5 dana Prikaži trenutno vrijeme Temperatura Šablona Postavke tema Naslov Mjerne jedinice Dodaj Sliku Korisničke postavke Z WCP OpenWeather kratki kôd WCP Weather ZSZ ZJZ Vrijeme Brzina vjetra Svoj grad možete pronaći na <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. tra atm kol bar pro velj pet hPa inHg sij srp lip kPa kgf/cm² kgf/m² km/h m/s ožu svi mbar mmHg pon milja/h stu lis psf psi sub ruj ned čet torr uto sri 
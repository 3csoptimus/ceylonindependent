��    o      �  �         `	     a	     s	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  !   �	     �	     �	  	   
     
     
     "
     2
     4
     8
     <
     U
     i
     }
     �
  *   �
  *   �
  �   �
     �     �     �  %  �     �     �     �     �     �  "   �     �     �                
                1     3     6     :     >     A     P     ]     d      m     �     �     �     �     �     �     �     �     �                    )     -     1  
   9  �   D     �     �     �     �     �     �     �     �     �                                   $     )     -     1     5     :     ?     C     G     K     O     S     W     [     _     c     g     l     p  c  t     �     �     �     �  
        $  
   9  	   D  	   N  	   X  	   b     l     p          �     �     �     �     �     �     �     �     �                $      E  !   f  	   �  -   �  -   �  �   �     �     �     �    �     �     �     �     �     �  *   �     �     �     �     �                    *     ,     /     3     7     :     V     l     t  (   �     �     �  
   �     �     �       	   	          "     3     5     O     [     _     c     j  �   o                              "     &     *     .     3     7     <     A     E     N     V     [     _     e     i     n     s     w     {          �     �     �     �     �     �     �     �     �                 #          S           	       K      %   6   ,   J   7       a   O       M   b      
            [              (      .         1       W   ?           \       j   R   h   N   E   !       8       4          i      U   n   Y         ]   <   _                 B      `       +   &   ^   -   9   ;   3   Q          @   0               I       X   H          l   m      :      o       e   >   d   g   /   A   L   "      F   D   2   Z       G                 C   '         V   =       $   k               P          c       T       *      )   5   f        "No Data" Message &deg;C &deg;F 0.5h - Not Recommended 12h 1h - Recommended 24h 2h 3h 6h 9h API API key Add Add new WCP OpenWeather shortcode Always Auto Detect City Name Current Theme Default Display Options E ENE ESE Enable "Google Maps API" Enable User Options Enable user options Expire User Options (days) General Hide Description of the Weather Conditions Hide description of the weather conditions How to get API key you can see <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">here</a>. Knots Language Location More information about the plugin you can find on the <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">plugin page</a> in the <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> and <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a> sections. <strong>Live demo</strong> you can find on <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">our site</a>. N NE NNE NNW NW Not supported in the current theme Options Other Pa Plugin Pressure Refresh Time Reset to Default S SE SSE SSW SW Save & Refresh Save Changes Select Settings Settings reset to default values Show 5 day forecast Show current weather Temperature Template Theme Settings Title Units Upload Image User Options W WCP OpenWeather Shortcode WCP Weather WNW WSW Weather Wind Speed You can find you city name on <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. apr atm aug bar dec feb fri hPa inHg jan jul jun kPa kgf/cm² kgf/m² km/h m/s mar may mbar mmHg mon mph nov oct psf psi sat sep sun thu torr tue wed Project-Id-Version: wcp-openweather
POT-Creation-Date: 2015-11-19 12:48+0300
PO-Revision-Date: 2015-11-19 12:55+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: assets
X-Poedit-SearchPath-1: classes
X-Poedit-SearchPath-2: config
X-Poedit-SearchPath-3: templates
 "Keine Daten" Bericht &deg;C &deg;F 0,5 stunde - Nicht empfohlen 12 stunden 1 stunde - Empfohlen 24 stunden 2 stunden 3 stunden 6 stunden 9 stunden API API-Schlüssel Hinzufügen Neuen WCP OpenWeather shortcode Immer Automatische Ortung Namen der Stadt Aktuelle Theme Standard Anzeigeoptionen O ONO OSO "Google Maps API" aktivieren Benutzereinstellungen aktivieren Benutzereinstellungen aktivieren Verfallen Benutzeroptionen (Tage) Allgemein Beschreibung der Wetterbedingungen verstecken Beschreibung der Wetterbedingungen verstecken Wie man ein API-Schlüssel erhalten kann, ist <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">hier</a> zu sehen. Knots Sprache Standort Sie finden mehr Informationen über den plugin am <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">plugin-seite</a>, im <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> und <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a> Sektionen. Die Demo finden Sie <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Die Demo">hier</a>. N NO NNO NNW NW Nicht in der aktuellen Themen unterstützt Optionen Andere Pa Plugin Druck Aktualisierungszeit Zurücksetzen S SO SSO SSW SW Speichern und Aktualisieren Änderungen Speichern Wählen Einstellungen Einstellungen auf Standard zurücksetzen 5-Tage-Vorhersage zeigen Aktuelle Wetter zeigen Temperatur Template Theme Einstellungen Titel Einheiten Bild hochladen Benutzeroptionen W WCP OpenWeather Shortcode WCP Weather WNW WSW Wetter Wind Den Namen der Stadt finden Sie auf <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. apr atm aug bar dez feb fre hPa inHg jan juli juni kPa kgf/cm² kgf/m² km/h m/s märz mai mbar mmHg mon mph nov okt psf psi sam sept son don torr die mit 
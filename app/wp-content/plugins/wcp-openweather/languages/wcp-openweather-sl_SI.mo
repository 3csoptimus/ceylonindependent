��    o      �  �         `	     a	     s	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  !   �	     �	     �	  	   
     
     
     "
     2
     4
     8
     <
     U
     i
     }
     �
  *   �
  *   �
  �   �
     �     �     �  %  �     �     �     �     �     �  "   �     �     �                
                1     3     6     :     >     A     P     ]     d      m     �     �     �     �     �     �     �     �     �                    )     -     1  
   9  �   D     �     �     �     �     �     �     �     �     �                                   $     )     -     1     5     :     ?     C     G     K     O     S     W     [     _     c     g     l     p  �  t          )     0     7     P     T     h     l     o     r     u     x  
   |     �     �     �     �     �     �     �     �     �     �     �     �          9  (   Y     �     �     �  �   �     Y     `     f  
  o     z     |          �     �     �  	   �     �     �     �     �     �     �     �     �     �     �     �     �            
      *   +     V     p     �     �     �     �     �     �     �     �     �     �               
       �        �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �               
                               $     (     ,     0     4     8     <     A     F     J                 #          S           	       K      %   6   ,   J   7       a   O       M   b      
            [              (      .         1       W   ?           \       j   R   h   N   E   !       8       4          i      U   n   Y         ]   <   _                 B      `       +   &   ^   -   9   ;   3   Q          @   0               I       X   H          l   m      :      o       e   >   d   g   /   A   L   "      F   D   2   Z       G                 C   '         V   =       $   k               P          c       T       *      )   5   f        "No Data" Message &deg;C &deg;F 0.5h - Not Recommended 12h 1h - Recommended 24h 2h 3h 6h 9h API API key Add Add new WCP OpenWeather shortcode Always Auto Detect City Name Current Theme Default Display Options E ENE ESE Enable "Google Maps API" Enable User Options Enable user options Expire User Options (days) General Hide Description of the Weather Conditions Hide description of the weather conditions How to get API key you can see <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">here</a>. Knots Language Location More information about the plugin you can find on the <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">plugin page</a> in the <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> and <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a> sections. <strong>Live demo</strong> you can find on <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">our site</a>. N NE NNE NNW NW Not supported in the current theme Options Other Pa Plugin Pressure Refresh Time Reset to Default S SE SSE SSW SW Save & Refresh Save Changes Select Settings Settings reset to default values Show 5 day forecast Show current weather Temperature Template Theme Settings Title Units Upload Image User Options W WCP OpenWeather Shortcode WCP Weather WNW WSW Weather Wind Speed You can find you city name on <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. apr atm aug bar dec feb fri hPa inHg jan jul jun kPa kgf/cm² kgf/m² km/h m/s mar may mbar mmHg mon mph nov oct psf psi sat sep sun thu torr tue wed Project-Id-Version: wcp-openweather
POT-Creation-Date: 2015-11-19 12:48+0300
PO-Revision-Date: 2015-11-19 13:04+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: sl_SI
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: assets
X-Poedit-SearchPath-1: classes
X-Poedit-SearchPath-2: config
X-Poedit-SearchPath-3: templates
 Sporočilo ''Brez podatkov'' &deg;C &deg;F 0.5h - Ni priporočljivo 12h 1h - Priporočljivo 24h 2h 3h 6h 9h API API ključ Dodaj Dodaj novo WCP OpenWeather kodo Vedno Samodejno zaznaj Mesto Trenutna tema Privzeto Prikaži možnosti E ENE ESE Omogoči "Google Maps API" Omogoči uporabniške možnosti Omogoči uporabniške možnosti Uporabniške možnosti so pretekle (dni) Osnovno Skrij opis vremenskih pogojev Skrij opis vremenskih pogojev Kako pridobite API ključ si poglejte <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">tukaj</a>. Vozlov Jezik Lokacija Več informacij o <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">vtičniku</a> lahko najdete na tej strani pod sekcijo <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> in <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a>. Demo verzijo najdete na <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Demo verzijo">naši strani</a>. N NE NNE NNW NW Nepodporto v tej temi Možnosti Drugo Pa Vtičnik Pritisk Osveži čas Ponastavi na privzeto S SE SSE SSW SW Shrani & osveži Shrani spremembe Izberi Nastavitve Ponastavi nastavitve na privzete vrednosti Prikaži 5 dnevno napoved Prikaži trenutno vreme Temperatura Predloga Nastavitve teme Naslov Enote Naloži sliko Uporabniške možnosti W WCP OpenWeather koda WCP Weather WNW WSW Vreme Hitrost vetra Svoje mesto lahko najdete na <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. apr atm avg bar dec feb pet hPa inHg jan jul jun kPa kgf/cm² kgf/m² km/h m/s mar maj mbar mmHg pon mph nov okt psf psi sob sep ned čet torr tor sre 
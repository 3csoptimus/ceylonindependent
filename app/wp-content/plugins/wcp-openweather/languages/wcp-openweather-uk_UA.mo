��    o      �  �         `	     a	     s	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  !   �	     �	     �	  	   
     
     
     "
     2
     4
     8
     <
     U
     i
     }
     �
  *   �
  *   �
  �   �
     �     �     �  %  �     �     �     �     �     �  "   �     �     �                
                1     3     6     :     >     A     P     ]     d      m     �     �     �     �     �     �     �     �     �                    )     -     1  
   9  �   D     �     �     �     �     �     �     �     �     �                                   $     )     -     1     5     :     ?     C     G     K     O     S     W     [     _     c     g     l     p  �  t  4   "     W     ^  '   e     �      �     �     �     �     �     �     �     �     �  6   �       +   *  
   V     a     y  #   �     �     �     �  $   �  4   	  4   >  ?   s     �  5   �  5   �  �   .  
   �     �     �  �       �     �     �     �  
   �  <      
   =     H     Q     V     c     l  8   �     �     �     �     �  	   �  $         %     6     E  W   ^  $   �     �     �       !        A     J  +   f  !   �     �     �     �     �     �     �     	  �   '     �     �     �     �     �     �          
               &     -     4     ;     I     U     ]     c     j     q     z     �     �     �     �     �     �     �     �     �     �     �                           #          S           	       K      %   6   ,   J   7       a   O       M   b      
            [              (      .         1       W   ?           \       j   R   h   N   E   !       8       4          i      U   n   Y         ]   <   _                 B      `       +   &   ^   -   9   ;   3   Q          @   0               I       X   H          l   m      :      o       e   >   d   g   /   A   L   "      F   D   2   Z       G                 C   '         V   =       $   k               P          c       T       *      )   5   f        "No Data" Message &deg;C &deg;F 0.5h - Not Recommended 12h 1h - Recommended 24h 2h 3h 6h 9h API API key Add Add new WCP OpenWeather shortcode Always Auto Detect City Name Current Theme Default Display Options E ENE ESE Enable "Google Maps API" Enable User Options Enable user options Expire User Options (days) General Hide Description of the Weather Conditions Hide description of the weather conditions How to get API key you can see <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">here</a>. Knots Language Location More information about the plugin you can find on the <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">plugin page</a> in the <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> and <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a> sections. <strong>Live demo</strong> you can find on <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">our site</a>. N NE NNE NNW NW Not supported in the current theme Options Other Pa Plugin Pressure Refresh Time Reset to Default S SE SSE SSW SW Save & Refresh Save Changes Select Settings Settings reset to default values Show 5 day forecast Show current weather Temperature Template Theme Settings Title Units Upload Image User Options W WCP OpenWeather Shortcode WCP Weather WNW WSW Weather Wind Speed You can find you city name on <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. apr atm aug bar dec feb fri hPa inHg jan jul jun kPa kgf/cm² kgf/m² km/h m/s mar may mbar mmHg mon mph nov oct psf psi sat sep sun thu torr tue wed Project-Id-Version: wcp-openweather
POT-Creation-Date: 2015-11-19 12:48+0300
PO-Revision-Date: 2015-11-19 13:09+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: uk_UA
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: assets
X-Poedit-SearchPath-1: classes
X-Poedit-SearchPath-2: config
X-Poedit-SearchPath-3: templates
 Повідомлення "Відсутні дані" &deg;C &deg;F 0.5г - Не рекомендовано 12г 1г - Рекомендовано 24г 2г 3г 6г 9г API Ключ API Додати Додати новий WCP OpenWeather шорткод Завжди Автоматичне визначення Місто Поточна тема За замовчуванням Опції відображення Сх Сх.Пвн.Сх Сх.Пвд.Сх Дозволити "Google Maps API" Дозволити опції користувача Дозволити опції користувача Зберігати опції користувача (днів) Головне Приховати опис погодних умов Приховати опис погодних умов Як отримати ключ API ви можете дізнатися <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">тут</a>. вузол Мова Розташування Більше інформації про плагін ви можете знайти на <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">сторінці плагіна</a> в <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> та в секції <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Скріншоти</a> . <strong>Демонстрацію наживо</strong> ви можете побачити на <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">нашому сайті</a>. Пвн Пвн.Сх Пвн.Пвн.Сх Пвн.Пвн.З Пвн.З. Не підтримується у поточній темі Опції Інше Па Плагін Тиск Час оновлення Налаштування за замовчуванням Пвд Пвд.Сх Пвд.Пвд.Сх Пвд.Пвд.З Пвд.З Зберегти та оновити Зберегти Вибрати Налаштування Налаштування скинуті до стану за замовчуванням П'ятиденний прогноз Поточний прогноз Температура Шаблон Налаштування теми Тема Одиниці виміру Завантажити зображення Опції користувача З WCP OpenWeather шорткод WCP Weather З.Пвн.З З.Пвд.З Погода Швидкість вітру Ви можете знайти ваше місто на <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. кві атм сер бар гру лют пт гПа д рт.ст. січ лип чер кПа кгс/см² кгс/м² км/г м/с бер тра мбар мм рт. ст. пн міль/г лис жов фунт. на кв. фут фунт. на кв. дюйм сб вер нд чт торр вт ср 
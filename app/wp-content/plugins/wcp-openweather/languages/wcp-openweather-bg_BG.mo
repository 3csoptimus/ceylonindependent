��    o      �  �         `	     a	     s	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  !   �	     �	     �	  	   
     
     
     "
     2
     4
     8
     <
     U
     i
     }
     �
  *   �
  *   �
  �   �
     �     �     �  %  �     �     �     �     �     �  "   �     �     �                
                1     3     6     :     >     A     P     ]     d      m     �     �     �     �     �     �     �     �     �                    )     -     1  
   9  �   D     �     �     �     �     �     �     �     �     �                                   $     )     -     1     5     :     ?     C     G     K     O     S     W     [     _     c     g     l     p  c  t  (   �            *        :     ?     C     H     L     P     T     X     \     i  0   v     �  '   �     �     �     �  "   
     -     0     7     >  >   ]  :   �  M   �     %  I   4  I   ~  �   �     �     �     �  �  �          �     �     �     �  '   �     �  
   �     �     �     �     	  3   %     Y     \     a     h     o     t     �     �     �  @   �  ,     $   C     h       $   �     �     �     �  %   �                3     ?     F  
   M      X  �   y     /     6     :     A     I     P  
   W     b     f     k     p     w     ~     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �  
                    #          S           	       K      %   6   ,   J   7       a   O       M   b      
            [              (      .         1       W   ?           \       j   R   h   N   E   !       8       4          i      U   n   Y         ]   <   _                 B      `       +   &   ^   -   9   ;   3   Q          @   0               I       X   H          l   m      :      o       e   >   d   g   /   A   L   "      F   D   2   Z       G                 C   '         V   =       $   k               P          c       T       *      )   5   f        "No Data" Message &deg;C &deg;F 0.5h - Not Recommended 12h 1h - Recommended 24h 2h 3h 6h 9h API API key Add Add new WCP OpenWeather shortcode Always Auto Detect City Name Current Theme Default Display Options E ENE ESE Enable "Google Maps API" Enable User Options Enable user options Expire User Options (days) General Hide Description of the Weather Conditions Hide description of the weather conditions How to get API key you can see <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">here</a>. Knots Language Location More information about the plugin you can find on the <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">plugin page</a> in the <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> and <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a> sections. <strong>Live demo</strong> you can find on <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">our site</a>. N NE NNE NNW NW Not supported in the current theme Options Other Pa Plugin Pressure Refresh Time Reset to Default S SE SSE SSW SW Save & Refresh Save Changes Select Settings Settings reset to default values Show 5 day forecast Show current weather Temperature Template Theme Settings Title Units Upload Image User Options W WCP OpenWeather Shortcode WCP Weather WNW WSW Weather Wind Speed You can find you city name on <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. apr atm aug bar dec feb fri hPa inHg jan jul jun kPa kgf/cm² kgf/m² km/h m/s mar may mbar mmHg mon mph nov oct psf psi sat sep sun thu torr tue wed Project-Id-Version: wcp-openweather
POT-Creation-Date: 2015-11-19 13:07+0300
PO-Revision-Date: 2015-11-19 13:08+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: bg_BG
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: assets
X-Poedit-SearchPath-1: classes
X-Poedit-SearchPath-2: config
X-Poedit-SearchPath-3: templates
 Съобщение "Няма данни" &deg;C &deg;F 30 мин - не се препотъчва 12ч 1ч 24ч 2ч 3ч 6ч 9ч API API ключ Добави Добави новият WCP OpenWeather код Винаги Автоматично засичане Град Текуща тема Основни Начин на показване И ИСИ ИЮИ Включи "Google Maps API" Включи потребителските настройки Включи потребителски настройки Време на потребителските настройки (в дни) Основни Скрий Описанието от Прогнозата за Време Скрий описанието от прогнозата за време Как да вземете вашият API ключ, можете да разберете <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">тук</a>. Възела Език Местонахождение Повече информация за разширението можете да откриете на <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">страницата на плугинът</a> в <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> и <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">скрийншот</a> секцията. <strong>Демо на плъгина</strong> можете да откриете на <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">нашият сайт</a>. С СИ ССИ ССЗ СЗ Темата не се поддържа Настройки Други Pa Разширение Налягане Обнови времето Връщане на базови настройки Ю ЮИ ЮЮИ ЮЮЗ ЮЗ Запази и обнови Запази промените Отбележи Настройки Настройките са върнати към начални Покажи 5 дневна прогноза Покажи текущо време Температура Тема Настройки на темата Заглавие Единици Прикачи снимка Потребителски опции З WCP OpenWeather шорткоуд WCP Weather ЗСЗ ЗЮЗ Време Скорост на вятъра Можете да намерите градът си на <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. Апр atm Авг барa Дек Фев Петък hpa inHg Ян Юли Юни kPa kgf/cm² kgf/m² km/h m/s Март Май mbar mmHg Пон mph Ноем Окт psf psi Съб Сеп Нед Четв torr Четв Сряда 
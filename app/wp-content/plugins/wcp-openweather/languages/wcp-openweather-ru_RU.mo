��    o      �  �         `	     a	     s	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  !   �	     �	     �	  	   
     
     
     "
     2
     4
     8
     <
     U
     i
     }
     �
  *   �
  *   �
  �   �
     �     �     �  %  �     �     �     �     �     �  "   �     �     �                
                1     3     6     :     >     A     P     ]     d      m     �     �     �     �     �     �     �     �     �                    )     -     1  
   9  �   D     �     �     �     �     �     �     �     �     �                                   $     )     -     1     5     :     ?     C     G     K     O     S     W     [     _     c     g     l     p  �  t  (   "     K     R  '   Y     �      �     �     �     �     �     �     �     �     �  :   �       3   &  
   Z     e     }  !   �     �     �     �  $   �  F   �  F   4  =   {     �  =   �  =     �   D     �     �       �  !     �     �     �     �     �  <   �  
         +     8     =     J     [  *   {     �     �     �     �     �  &   �     �     �     
  Q     %   o     �     �     �     �     �  !     )   (  #   R     v     y     �     �     �     �     �  �   �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �                         !     *     ;     @     L     S     Z     v     �     �     �     �     �     �     �                 #          S           	       K      %   6   ,   J   7       a   O       M   b      
            [              (      .         1       W   ?           \       j   R   h   N   E   !       8       4          i      U   n   Y         ]   <   _                 B      `       +   &   ^   -   9   ;   3   Q          @   0               I       X   H          l   m      :      o       e   >   d   g   /   A   L   "      F   D   2   Z       G                 C   '         V   =       $   k               P          c       T       *      )   5   f        "No Data" Message &deg;C &deg;F 0.5h - Not Recommended 12h 1h - Recommended 24h 2h 3h 6h 9h API API key Add Add new WCP OpenWeather shortcode Always Auto Detect City Name Current Theme Default Display Options E ENE ESE Enable "Google Maps API" Enable User Options Enable user options Expire User Options (days) General Hide Description of the Weather Conditions Hide description of the weather conditions How to get API key you can see <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">here</a>. Knots Language Location More information about the plugin you can find on the <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">plugin page</a> in the <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> and <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a> sections. <strong>Live demo</strong> you can find on <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">our site</a>. N NE NNE NNW NW Not supported in the current theme Options Other Pa Plugin Pressure Refresh Time Reset to Default S SE SSE SSW SW Save & Refresh Save Changes Select Settings Settings reset to default values Show 5 day forecast Show current weather Temperature Template Theme Settings Title Units Upload Image User Options W WCP OpenWeather Shortcode WCP Weather WNW WSW Weather Wind Speed You can find you city name on <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. apr atm aug bar dec feb fri hPa inHg jan jul jun kPa kgf/cm² kgf/m² km/h m/s mar may mbar mmHg mon mph nov oct psf psi sat sep sun thu torr tue wed Project-Id-Version: wcp-openweather
POT-Creation-Date: 2015-11-19 12:48+0300
PO-Revision-Date: 2015-11-19 13:14+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: assets
X-Poedit-SearchPath-1: classes
X-Poedit-SearchPath-2: config
X-Poedit-SearchPath-3: templates
 Сообщение "Нет данных" &deg;C &deg;F 0.5ч - Не рекомендуется 12ч 1ч - Рекомендуется 24ч 2ч 3ч 6ч 9ч API Ключ API Добавить Добавить новый WCP OpenWeather шорткод Всегда Автоматическое определение Город Текущая тема По умолчанию Опции отображения В ВСВ ВЮВ Отключить "Google Maps API" Разрешить пользовательскую настройку Разрешить пользовательскую настройку Хранить опции пользователя (дней) Главное Скрыть описание погодных условий Скрыть описание погодных условий Как получить ключ API вы можете узнать <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">тут</a>. узел Язык Расположение Больше информации о плагине вы можете найти на <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">странице плагина</a> в <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> и в секции <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Скриншоты</a> . <strong>Живую демонстрацию</strong> вы можете посмотреть на <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">нашем сайте</a>. С СВ ССВ ССЗ СЗ Не поддерживается в текущей теме Опции Другое Па Плагин Давление Время обновления Настройки по умолчанию Ю ЮВ ЮЮВ ЮЮЗ ЮЗ Сохранить и обновить Сохранить Выбрать Настройки Настройки сброшены в состояние по умолчанию Пятидневный прогноз Текущий прогноз Температура Шаблон Настройки темы Заголовок Единицы измерения Загрузить изображение Опции пользователя З WCP OpenWeather шорткод WCP Weather ЗСЗ ЗЮЗ Погода Скорость ветра Вы можете найти ваш город на <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. апр атм авг бар дек фев пт гПа д рт.ст. янв июл июн кПа кгс/см² кгс/м² км/ч м/с мар май мбар мм рт. ст. пн миль/ч ноя окт фунт. на кв. фут фунт. на кв. дюйм сб сен вс чт торр вт ср 
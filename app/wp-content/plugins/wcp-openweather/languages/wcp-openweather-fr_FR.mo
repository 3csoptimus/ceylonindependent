��    o      �  �         `	     a	     s	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  !   �	     �	     �	  	   
     
     
     "
     2
     4
     8
     <
     U
     i
     }
     �
  *   �
  *   �
  �   �
     �     �     �  %  �     �     �     �     �     �  "   �     �     �                
                1     3     6     :     >     A     P     ]     d      m     �     �     �     �     �     �     �     �     �                    )     -     1  
   9  �   D     �     �     �     �     �     �     �     �     �                                   $     )     -     1     5     :     ?     C     G     K     O     S     W     [     _     c     g     l     p  b  t     �     �     �     �               (     ,     /     2     5     8     <     E  ,   M     z     �     �     �     �     �     �     �     �     �  $      $   %  #   J  	   n  6   x  6   �  �   �     �     �     �  d  �                           (        D     L     R  	   U     _     h     y     �     �     �     �     �     �     �     �     �  6   �          1     N     [     c     v     |     �     �     �     �     �     �     �     �     �  �   �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �                
                              "     &     +     /     3     8     <                 #          S           	       K      %   6   ,   J   7       a   O       M   b      
            [              (      .         1       W   ?           \       j   R   h   N   E   !       8       4          i      U   n   Y         ]   <   _                 B      `       +   &   ^   -   9   ;   3   Q          @   0               I       X   H          l   m      :      o       e   >   d   g   /   A   L   "      F   D   2   Z       G                 C   '         V   =       $   k               P          c       T       *      )   5   f        "No Data" Message &deg;C &deg;F 0.5h - Not Recommended 12h 1h - Recommended 24h 2h 3h 6h 9h API API key Add Add new WCP OpenWeather shortcode Always Auto Detect City Name Current Theme Default Display Options E ENE ESE Enable "Google Maps API" Enable User Options Enable user options Expire User Options (days) General Hide Description of the Weather Conditions Hide description of the weather conditions How to get API key you can see <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">here</a>. Knots Language Location More information about the plugin you can find on the <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">plugin page</a> in the <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> and <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a> sections. <strong>Live demo</strong> you can find on <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">our site</a>. N NE NNE NNW NW Not supported in the current theme Options Other Pa Plugin Pressure Refresh Time Reset to Default S SE SSE SSW SW Save & Refresh Save Changes Select Settings Settings reset to default values Show 5 day forecast Show current weather Temperature Template Theme Settings Title Units Upload Image User Options W WCP OpenWeather Shortcode WCP Weather WNW WSW Weather Wind Speed You can find you city name on <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. apr atm aug bar dec feb fri hPa inHg jan jul jun kPa kgf/cm² kgf/m² km/h m/s mar may mbar mmHg mon mph nov oct psf psi sat sep sun thu torr tue wed Project-Id-Version: wcp-openweather
POT-Creation-Date: 2015-11-19 12:48+0300
PO-Revision-Date: 2015-11-19 12:58+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: assets
X-Poedit-SearchPath-1: classes
X-Poedit-SearchPath-2: config
X-Poedit-SearchPath-3: templates
 Message "Aucune Donnée" &deg;C &deg;F 0.5h - Déconseillé 12h 1h - Recommandé 24h 2h 3h 6h 9h API Clé API Ajouter Ajouter un nouveau WCP OpenWeather shortcode Toujours Détection Automatique Nom De La Ville Thème Actuel Par Défaut Options d'Affichage E ENE ESE Activer les "Google Maps API" Activer les options de l'utilisateur Activer les options de l'utilisateur Expirez Options Utilisateur (jours) Principal Cacher la description des conditions météorologiques Cacher la description des conditions météorologiques Comment faire pour obtenir la clé API que vous pouvez voir <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">ici</a>. Knots Langue Emplacement Plus d'informations sur le plugin vous pouvez trouver sur la <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">page du plugin</a> dans les <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> et <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Captures d'écran">Captures d'écran</a> sections. <strong>Démonstration en direct</strong> vous pouvez trouver sur <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Démonstration en direct">notre site</a>. N NE NNE NNO NO Non pris en charge dans le thème actuel Options Autre Pa Extension Pression Actualiser Temps Réinitialiser S SE SSE SSO SO Sauvegarder & Refresh Sauvegarder Les Changements Choisir Paramètres Réglages de réinitialisation aux valeurs par défaut Voir prévisions à 5 jours Afficher la météo actuelle Température Modèle Paramètres thème Titre Unités Envoyer une image Options utilisateur O WCP OpenWeather Shortcode WCP Weather ONO OSO Météo Vitesse du vent Vous pouvez vous trouver le nom de la ville sur <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. avril atm août bar déc févr ven hPa inHg janv juil juin kPa kgf/cm² kgf/m² km/h m/s mars mai mbar mmHg lun mph nov oct psf psi sam sept dim jeu torr mar mer 
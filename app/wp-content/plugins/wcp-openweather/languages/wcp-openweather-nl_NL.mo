��    o      �  �         `	     a	     s	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  !   �	     �	     �	  	   
     
     
     "
     2
     4
     8
     <
     U
     i
     }
     �
  *   �
  *   �
  �   �
     �     �     �  %  �     �     �     �     �     �  "   �     �     �                
                1     3     6     :     >     A     P     ]     d      m     �     �     �     �     �     �     �     �     �                    )     -     1  
   9  �   D     �     �     �     �     �     �     �     �     �                                   $     )     -     1     5     :     ?     C     G     K     O     S     W     [     _     c     g     l     p  c  t     �     �     �                     ,     0     3     6     9     <     @  	   L  *   V     �     �  
   �     �  	   �     �     �     �     �     �     �       $   3     X  /   a  /   �  �   �     X     _     d    k     �     �     �     �     �  %   �     �     �     �     �  	   �     �     �                                   .     B     I  .   V     �     �     �     �     �     �     �     �                    0     <     @     D     I  �   V     �     �     �               
                              #     '     +     4     <     A     E     I     M     R     W     [     _     c     g     k     o     s     w     {          �     �                 #          S           	       K      %   6   ,   J   7       a   O       M   b      
            [              (      .         1       W   ?           \       j   R   h   N   E   !       8       4          i      U   n   Y         ]   <   _                 B      `       +   &   ^   -   9   ;   3   Q          @   0               I       X   H          l   m      :      o       e   >   d   g   /   A   L   "      F   D   2   Z       G                 C   '         V   =       $   k               P          c       T       *      )   5   f        "No Data" Message &deg;C &deg;F 0.5h - Not Recommended 12h 1h - Recommended 24h 2h 3h 6h 9h API API key Add Add new WCP OpenWeather shortcode Always Auto Detect City Name Current Theme Default Display Options E ENE ESE Enable "Google Maps API" Enable User Options Enable user options Expire User Options (days) General Hide Description of the Weather Conditions Hide description of the weather conditions How to get API key you can see <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">here</a>. Knots Language Location More information about the plugin you can find on the <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">plugin page</a> in the <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> and <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a> sections. <strong>Live demo</strong> you can find on <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">our site</a>. N NE NNE NNW NW Not supported in the current theme Options Other Pa Plugin Pressure Refresh Time Reset to Default S SE SSE SSW SW Save & Refresh Save Changes Select Settings Settings reset to default values Show 5 day forecast Show current weather Temperature Template Theme Settings Title Units Upload Image User Options W WCP OpenWeather Shortcode WCP Weather WNW WSW Weather Wind Speed You can find you city name on <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. apr atm aug bar dec feb fri hPa inHg jan jul jun kPa kgf/cm² kgf/m² km/h m/s mar may mbar mmHg mon mph nov oct psf psi sat sep sun thu torr tue wed Project-Id-Version: wcp-openweather
POT-Creation-Date: 2015-11-19 12:48+0300
PO-Revision-Date: 2015-11-19 13:02+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: assets
X-Poedit-SearchPath-1: classes
X-Poedit-SearchPath-2: config
X-Poedit-SearchPath-3: templates
 "Geen gegevens" boodschap &deg;C &deg;F 0.5u - Niet aanbevolen 12u 1u - Recommended 24u 2u 3u 6u 9u API API sleutel Toevoegen Nieuwe WCP OpenWeather shortcode toevoegen Altijd Automatisch Detecteren Plaatsnaam Huidig thema Standaard Weergave opties O ONO OZO "Google Maps API" inschakelen Gebruikersopties inschakelen Gebruikersopties inschakelen Verstrijken gebruikersopties (dagen) Algemeen Verberg omschrijving van de weersomstandigheden Verberg omschrijving van de weersomstandigheden Hoe een API sleutel verkrijgen kan je <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">hier</a> zien. Knopen Taal Plaats Meer info over de plugin kan je vinden op de <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">plugin pagina</a> in de <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> en <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a> secties. Je kan een <strong>live demo</strong> vinden op <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="onze sie">onze sie</a>. N NO NNO NNW NW Niet ondersteund in het huidige thema Opties Andere Pa Plugin Luchtdruk Ververs tijd Standaardwaarden terugzetten Z ZO ZZO ZZW ZW Bewaar & ververs Wijzigingen opslaan Kiezen Instellingen Instellingen terugzetten naar standaardwaarden Toon voorspelling voor 5 dagen Toon het huidige weer Temperatuur Sjabloon Thema instellingen Titel Eenheden Upload Afbeelding Gebruikersopties W WCP OpenWeather Shortcode WCP Weather WNW WZW Weer Windsnelheid Je kan je plaatsnaam terugvinden op <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. apr atm aug bar dec feb vri hPa inHg jan jul jun kPa kgf/cm² kgf/m² km/u m/s maa mei mbar mmHg maa mph nov okt psf psi zat sep zon don torr din woe 
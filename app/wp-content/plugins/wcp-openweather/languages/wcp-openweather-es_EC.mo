��    o      �  �         `	     a	     s	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  !   �	     �	     �	  	   
     
     
     "
     2
     4
     8
     <
     U
     i
     }
     �
  *   �
  *   �
  �   �
     �     �     �  %  �     �     �     �     �     �  "   �     �     �                
                1     3     6     :     >     A     P     ]     d      m     �     �     �     �     �     �     �     �     �                    )     -     1  
   9  �   D     �     �     �     �     �     �     �     �     �                                   $     )     -     1     5     :     ?     C     G     K     O     S     W     [     _     c     g     l     p  c  t     �     �     �                     +     /     2     5     8     ;     ?     G  '   O     w          �     �     �     �     �     �     �     �     �     �  #        ?  4   G  4   |  �   �     3     9  	   B  a  L     �     �     �     �     �     �     �     �     �     �     �     �          %     '     *     .     2     5     I     Y     e  ,   u     �     �     �  	   �     �                    )     =     ?     Y     e     i     m     s  �   �     0     4     8     >     B     F     J     O     S     X     [     _     c     g     p     x     }     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �                 #          S           	       K      %   6   ,   J   7       a   O       M   b      
            [              (      .         1       W   ?           \       j   R   h   N   E   !       8       4          i      U   n   Y         ]   <   _                 B      `       +   &   ^   -   9   ;   3   Q          @   0               I       X   H          l   m      :      o       e   >   d   g   /   A   L   "      F   D   2   Z       G                 C   '         V   =       $   k               P          c       T       *      )   5   f        "No Data" Message &deg;C &deg;F 0.5h - Not Recommended 12h 1h - Recommended 24h 2h 3h 6h 9h API API key Add Add new WCP OpenWeather shortcode Always Auto Detect City Name Current Theme Default Display Options E ENE ESE Enable "Google Maps API" Enable User Options Enable user options Expire User Options (days) General Hide Description of the Weather Conditions Hide description of the weather conditions How to get API key you can see <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">here</a>. Knots Language Location More information about the plugin you can find on the <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">plugin page</a> in the <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="FAQ">FAQ</a> and <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Screenshots">Screenshots</a> sections. <strong>Live demo</strong> you can find on <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Live Demo">our site</a>. N NE NNE NNW NW Not supported in the current theme Options Other Pa Plugin Pressure Refresh Time Reset to Default S SE SSE SSW SW Save & Refresh Save Changes Select Settings Settings reset to default values Show 5 day forecast Show current weather Temperature Template Theme Settings Title Units Upload Image User Options W WCP OpenWeather Shortcode WCP Weather WNW WSW Weather Wind Speed You can find you city name on <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. apr atm aug bar dec feb fri hPa inHg jan jul jun kPa kgf/cm² kgf/m² km/h m/s mar may mbar mmHg mon mph nov oct psf psi sat sep sun thu torr tue wed Project-Id-Version: wcp-openweather
POT-Creation-Date: 2015-11-19 12:48+0300
PO-Revision-Date: 2015-11-19 12:56+0300
Last-Translator: - <->
Language-Team: Webcodin <info@webcodin.com>
Language: es_EC
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: assets
X-Poedit-SearchPath-1: classes
X-Poedit-SearchPath-2: config
X-Poedit-SearchPath-3: templates
 Mensaje "No información" &deg;C &deg;F 0.5h - No recomendado 12h 1h - Recomendado 24h 2h 3h 6h 9h API API Key Agregar Agregar nuevo WCP OpenWeather shortcode Siempre Auto Detectar Ciudad Tema actual Default Mostrar Opciones E ENE ESE Habilitar "Google Maps API" Habilitar opciones de usuario Habilitar opciones de usuario Expirar opciones de usuario (días) General No mostrar descripción de las condiciones del Clima No mostrar descripción de las condiciones del Clima Como obtener API Key: <a href="http://openweathermap.org/appid" title="http://openweathermap.org/appid" target="_blank">aqui</a>. Nudos Lenguaje Localidad Más información sobre el plugin se puede encontrar en la <a href="https://wordpress.org/plugins/wcp-openweather/" target="_blank" title="wordpress.org">página del plugin</a> en el <a href="https://wordpress.org/plugins/wcp-openweather/faq/" target="_blank" title="Preguntas más frecuentes">Preguntas más frecuentes</a> y <a href="https://wordpress.org/plugins/wcp-openweather/screenshots/" target="_blank" title="Capturas">Capturas</a> secciones. Demostración en directo se puede encontrar en <a href="http://wpdemo.webcodin.com/weather-forecast/" target="_blank" title="Demo en vivo">nuestro sitio</a>. N NE NNE NNO NO No soportado in esta plantilla Opciones Otros Pa Plugin Presión Actualizar tiempo Reajustar al inicio S SE SSE SSO SO Grabar y actualizar Grabar Cambiios Seleccionar Configuraciones Configuraciones: ajustar a valores de inicio Mostar pronóstico de 5 días Mostrar clima actual Temperatura Plantilla Configuraciones de la plantilla Título Unidades Subir imagen Opciones de usuario O WCP OpenWeather Shortcode WCP Weather ONO OSO Clima Velocidad del viento Ud. Puede encontrar el nombre de ciudad en: <a href="http://www.openweathermap.com/" title="http://www.openweathermap.com/" target="_blank">www.openweathermap.com</a>. abr atm agost bar dic fev vier hPa inHg en jul jun kPa kgf/cm² kgf/m² km/h m/s mar may mbar mmHg lun mph nov oct psf psi sab sept dom jue torr mar mier 
msgid ""
msgstr ""
"Project-Id-Version: Email posts to subscribers\n"
"POT-Creation-Date: 2015-12-08 21:57+0800\n"
"PO-Revision-Date: 2015-12-08 21:57+0800\n"
"Last-Translator: \n"
"Language-Team: www.gopiplus.com\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.7\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../classes/loadwidget.php:45 ../subscribers/view-subscriber-page.php:20
#: ../subscribers/view-subscriber-page.php:30
#: ../subscribers/view-subscriber-show.php:227
#: ../subscribers/view-subscriber-show.php:239 ../widget/widget.php:23
msgid "Name"
msgstr "Nazwisko"

#: ../classes/loadwidget.php:50 ../widget/widget.php:28
msgid "Email *"
msgstr "E-mail *"

#: ../classes/loadwidget.php:55 ../widget/widget.php:33
msgid "Subscribe"
msgstr "Subskrybuj"

#: ../classes/register.php:43
msgid "These tables could not be created on installation "
msgstr "Tabele te nie mogą być tworzone na instalacji"

#: ../classes/register.php:70 ../classes/register.php:71
msgid "Email Posts"
msgstr "E-mail Posty"

#: ../classes/register.php:73 ../classes/register.php:74
msgid "Subscribers"
msgstr "Subskrybenci"

#: ../classes/register.php:76 ../classes/register.php:77
msgid "Templates"
msgstr "Szablony"

#: ../classes/register.php:79 ../classes/register.php:80
#: ../configuration/configuration-show.php:46
msgid "Mail Configuration"
msgstr "Konfiguracja poczty"

#: ../classes/register.php:82 ../classes/register.php:83
#: ../sendmail/sendmail-subscriber.php:204
#: ../sendmail/sendmail-subscriber.php:206
msgid "Send Email"
msgstr "Wyślij link na email"

#: ../classes/register.php:85 ../classes/register.php:86
#: ../settings/settings-edit.php:195
msgid "Settings"
msgstr "Ustawienia"

#: ../classes/register.php:88 ../classes/register.php:89
msgid "Schedule Email"
msgstr "Harmonogram email"

#: ../classes/register.php:91 ../classes/register.php:92
msgid "Sent Report"
msgstr "Wysłane Zgłoś"

#: ../classes/register.php:94 ../classes/register.php:95 ../help/help.php:5
msgid "Help & Info"
msgstr "Pomoc i Info"

#: ../classes/register.php:159
msgid "Widget Title"
msgstr "Tytuł widgetu"

#: ../classes/register.php:163
msgid "Name Field"
msgstr "pole nazwy"

#: ../classes/register.php:170
msgid "Short Description"
msgstr "Krótki opis"

#: ../configuration/configuration-add.php:30
#: ../configuration/configuration-add.php:101
#: ../configuration/configuration-edit.php:44
#: ../configuration/configuration-edit.php:98
msgid "Please enter mail subject."
msgstr "Podaj swój temat wiadomości."

#: ../configuration/configuration-add.php:51
msgid "Mail configuration was successfully created."
msgstr "Konfiguracja poczty został utworzony."

#: ../configuration/configuration-add.php:85
#: ../configuration/configuration-edit.php:84 ../settings/schedule-show.php:9
#: ../settings/schedule-show.php:10 ../settings/schedule-show.php:11
#: ../settings/settings-edit.php:178 ../subscribers/view-subscriber-add.php:74
#: ../subscribers/view-subscriber-edit.php:76
#: ../subscribers/view-subscriber-import.php:79
#: ../subscribers/view-subscriber-import.php:109
#: ../template/template-add.php:70 ../template/template-edit.php:76
msgid "Click here"
msgstr "Kliknij tutaj"

#: ../configuration/configuration-add.php:85
#: ../configuration/configuration-edit.php:84
#: ../settings/settings-edit.php:179 ../subscribers/view-subscriber-add.php:74
#: ../subscribers/view-subscriber-edit.php:76
#: ../subscribers/view-subscriber-import.php:79
#: ../subscribers/view-subscriber-import.php:109
#: ../template/template-add.php:71 ../template/template-edit.php:77
msgid " to view the details"
msgstr "aby zobaczyć szczegóły"

#: ../configuration/configuration-add.php:97
msgid "Add configuration"
msgstr "Dodaj konfigurację"

#: ../configuration/configuration-add.php:99
#: ../configuration/configuration-edit.php:96
msgid "Mail subject"
msgstr "Temat mail"

#: ../configuration/configuration-add.php:103
#: ../configuration/configuration-edit.php:100
#: ../configuration/configuration-show.php:65
#: ../configuration/configuration-show.php:78
msgid "Template"
msgstr "Szablon"

#: ../configuration/configuration-add.php:118
#: ../configuration/configuration-edit.php:121
msgid "Please select template for this configuration."
msgstr "Proszę wybrać szablon dla tej konfiguracji."

#: ../configuration/configuration-add.php:120
#: ../configuration/configuration-cron.php:112
#: ../configuration/configuration-edit.php:123
msgid "Post Details"
msgstr "Wpisz szczegóły"

#: ../configuration/configuration-add.php:121
#: ../configuration/configuration-edit.php:124
msgid "Post count."
msgstr "Liczba wpisów"

#: ../configuration/configuration-add.php:132
#: ../configuration/configuration-edit.php:135
msgid "Number of post to add in the email."
msgstr "Ilość postu, aby dodać w e-mailu."

#: ../configuration/configuration-add.php:134
#: ../configuration/configuration-edit.php:137
msgid "Post categories"
msgstr "Kategorie wpisów."

#: ../configuration/configuration-add.php:136
#: ../configuration/configuration-edit.php:139
msgid "Please enter category IDs, separated by commas."
msgstr "Podaj identyfikatorów kategorii, rozdzielając je przecinkami."

#: ../configuration/configuration-add.php:138
#: ../configuration/configuration-edit.php:141
msgid "Post orderbys"
msgstr "Orderbys Pocztowe"

#: ../configuration/configuration-add.php:147
#: ../configuration/configuration-edit.php:150
msgid "Select your post orderbys"
msgstr "Wybierz swoje orderbys pocztowe"

#: ../configuration/configuration-add.php:149
#: ../configuration/configuration-edit.php:152
msgid "Post order"
msgstr "Uporządkuj Post:"

#: ../configuration/configuration-add.php:154
#: ../configuration/configuration-edit.php:157
msgid "Select your post order"
msgstr "Wybierz zamówienia pocztowego"

#: ../configuration/configuration-add.php:157
#: ../configuration/configuration-edit.php:160
msgid "Mail Setting"
msgstr "Ustawienia poczty"

#: ../configuration/configuration-add.php:159
#: ../configuration/configuration-edit.php:162
msgid "Mail count for one shot."
msgstr "Liczba wiadomości na jeden strzał."

#: ../configuration/configuration-add.php:172
#: ../configuration/configuration-edit.php:175
msgid "How many emails you want to send at one shot"
msgstr "Ile maile chcesz wysłać w jednym ujęciu"

#: ../configuration/configuration-add.php:174
#: ../configuration/configuration-edit.php:177
msgid "Add unsubscribe link"
msgstr "Dodaj wypisz związek"

#: ../configuration/configuration-add.php:178
#: ../configuration/configuration-edit.php:181
msgid "Do you want to add unsubscribe link with this mail configuration."
msgstr "Czy chcesz dodać odnośnik wypisać z tej konfiguracji poczty."

#: ../configuration/configuration-add.php:180
#: ../configuration/configuration-edit.php:183
msgid "View status (BETA)"
msgstr "Wyświetl stan (BETA)"

#: ../configuration/configuration-add.php:184
#: ../configuration/configuration-edit.php:187
msgid "Like to track whether that email is viewed or not?"
msgstr "Jak śledzić, czy e-mail jest postrzegana, czy nie?"

#: ../configuration/configuration-add.php:189
#: ../subscribers/view-subscriber-add.php:108 ../template/template-add.php:105
msgid "Insert Details"
msgstr "Wstaw Szczegóły"

#: ../configuration/configuration-add.php:190
#: ../configuration/configuration-edit.php:193
#: ../sendmail/sendmail-subscriber.php:208 ../settings/settings-edit.php:385
#: ../subscribers/view-subscriber-add.php:109
#: ../subscribers/view-subscriber-edit.php:109
#: ../template/template-add.php:106 ../template/template-edit.php:114
msgid "Cancel"
msgstr "Anuluj"

#: ../configuration/configuration-add.php:191
#: ../configuration/configuration-cron.php:128
#: ../configuration/configuration-edit.php:194
#: ../configuration/configuration-preview.php:39
#: ../configuration/configuration-show.php:170
#: ../sendmail/sendmail-subscriber.php:209
#: ../sentmail/deliverreport-show.php:111 ../sentmail/sentmail-preview.php:31
#: ../settings/settings-edit.php:386
#: ../subscribers/view-subscriber-add.php:110
#: ../subscribers/view-subscriber-edit.php:110
#: ../subscribers/view-subscriber-export.php:73
#: ../subscribers/view-subscriber-import.php:131
#: ../subscribers/view-subscriber-page.php:72
#: ../subscribers/view-subscriber-show.php:216
#: ../subscribers/view-subscriber-show.php:341
#: ../template/template-add.php:107 ../template/template-edit.php:115
#: ../template/template-preview.php:29 ../template/template-show.php:116
msgid "Help"
msgstr "Pomoc"

#: ../configuration/configuration-cron.php:12
#: ../configuration/configuration-edit.php:11
#: ../configuration/configuration-preview.php:10
#: ../configuration/configuration-show.php:16
#: ../sentmail/sentmail-preview.php:12 ../sentmail/sentmail-show.php:18
#: ../settings/settings-edit.php:11 ../subscribers/view-subscriber-edit.php:16
#: ../subscribers/view-subscriber-show.php:39 ../template/template-edit.php:11
#: ../template/template-preview.php:10 ../template/template-show.php:16
msgid "Oops, selected details doesnt exist."
msgstr "Ups, wybrane szczegóły nie istnieją."

#: ../configuration/configuration-cron.php:46
msgid "Total Emails"
msgstr "Wszystkich wiadomości e-mail"

#: ../configuration/configuration-cron.php:56
msgid "Total valid email address in our database :"
msgstr "Razem poprawny adres e-mail w naszej bazie danych:"

#: ../configuration/configuration-cron.php:60
#: ../configuration/configuration-show.php:102
msgid "Cron Links"
msgstr "Cron menu"

#: ../configuration/configuration-cron.php:64
msgid ""
"Note: Email will be retrieved based on Database ID (i.e. Oldest email come "
"first)"
msgstr ""
"Uwaga: Adres e-mail będą pobierane na podstawie ID bazy danych (czyli "
"starszy e-mail na pierwszym miejscu)"

#: ../configuration/configuration-cron.php:66
msgid "Cron link for first"
msgstr "Link Cron za pierwszy"

#: ../configuration/configuration-cron.php:66
#: ../configuration/configuration-cron.php:72
#: ../configuration/configuration-cron.php:78
#: ../configuration/configuration-cron.php:84
#: ../configuration/configuration-cron.php:90
msgid "email"
msgstr "e-mail"

#: ../configuration/configuration-cron.php:68
#: ../configuration/configuration-cron.php:74
#: ../configuration/configuration-cron.php:80
#: ../configuration/configuration-cron.php:86
#: ../configuration/configuration-cron.php:92
msgid "click here to view emails"
msgstr "kliknij tutaj, aby wyświetlić wiadomości e-mail"

#: ../configuration/configuration-cron.php:72
msgid "Cron link for second"
msgstr "Link Cron na sekundę"

#: ../configuration/configuration-cron.php:78
msgid "Cron link for third"
msgstr "Link Cron do trzeciej"

#: ../configuration/configuration-cron.php:84
msgid "Cron link for fourth"
msgstr "Link Cron za czwarty"

#: ../configuration/configuration-cron.php:90
msgid "Cron link for fifth"
msgstr "Link Cron do piątej"

#: ../configuration/configuration-cron.php:98
msgid "Template Details"
msgstr "Szablon Szczegóły"

#: ../configuration/configuration-cron.php:127
#: ../configuration/configuration-preview.php:37
#: ../sentmail/deliverreport-show.php:110 ../sentmail/sentmail-preview.php:30
#: ../subscribers/view-subscriber-export.php:72
#: ../subscribers/view-subscriber-import.php:130
#: ../subscribers/view-subscriber-page.php:71
#: ../template/template-preview.php:27
msgid "Back"
msgstr "Wstecz"

#: ../configuration/configuration-edit.php:65
msgid "Mail configuration was successfully updated."
msgstr "Konfiguracja poczty został pomyślnie zaktualizowany."

#: ../configuration/configuration-edit.php:94
msgid "Edit configuration"
msgstr "Edytuj konfigurację główną"

#: ../configuration/configuration-edit.php:192
#: ../subscribers/view-subscriber-edit.php:108
#: ../template/template-edit.php:113
msgid "Update Details"
msgstr "aktualizacja danych szczegółowych"

#: ../configuration/configuration-preview.php:26
msgid "Preview Mail"
msgstr "Podgląd Poczta"

#: ../configuration/configuration-preview.php:38
#: ../configuration/configuration-show.php:101
#: ../subscribers/view-subscriber-show.php:272
#: ../template/template-preview.php:28 ../template/template-show.php:89
msgid "Edit"
msgstr "Edytuj"

#: ../configuration/configuration-show.php:31 ../sentmail/sentmail-show.php:33
#: ../subscribers/view-subscriber-show.php:56
#: ../subscribers/view-subscriber-show.php:100
#: ../template/template-show.php:31
msgid "Selected record was successfully deleted."
msgstr "Wybrany rekord został pomyślnie usunięty."

#: ../configuration/configuration-show.php:47
#: ../configuration/configuration-show.php:169
#: ../subscribers/view-subscriber-show.php:184
#: ../subscribers/view-subscriber-show.php:213
#: ../subscribers/view-subscriber-show.php:338
#: ../template/template-show.php:46
msgid "Add New"
msgstr "Dodaj nowy"

#: ../configuration/configuration-show.php:64
#: ../configuration/configuration-show.php:77
msgid "Mail Subject"
msgstr "Mail Temat"

#: ../configuration/configuration-show.php:66
#: ../configuration/configuration-show.php:79
msgid "Send Count"
msgstr "Wyślij hrabiego"

#: ../configuration/configuration-show.php:67
#: ../configuration/configuration-show.php:80
msgid "No of Post"
msgstr "Nie Poczty"

#: ../configuration/configuration-show.php:68
#: ../configuration/configuration-show.php:81
msgid "Post Category"
msgstr "Kategoria wpisu"

#: ../configuration/configuration-show.php:69
#: ../configuration/configuration-show.php:82
msgid "Orderby"
msgstr "Sortuj wg:"

#: ../configuration/configuration-show.php:70
#: ../configuration/configuration-show.php:83
msgid "Order"
msgstr "Kolejność"

#: ../configuration/configuration-show.php:71
#: ../configuration/configuration-show.php:84
#: ../configuration/configuration-show.php:124
msgid "Cron Details"
msgstr "Cron Szczegóły"

#: ../configuration/configuration-show.php:103
msgid "Send Mail"
msgstr "Wyślij e-mail"

#: ../configuration/configuration-show.php:104
#: ../template/template-show.php:60 ../template/template-show.php:69
#: ../template/template-show.php:96
msgid "Preview"
msgstr "Podgląd"

#: ../configuration/configuration-show.php:107
#: ../subscribers/view-subscriber-show.php:275
#: ../subscribers/view-subscriber-show.php:317
#: ../template/template-show.php:91
msgid "Delete"
msgstr "Usuń"

#: ../configuration/configuration-show.php:133
#: ../sentmail/deliverreport-show.php:75 ../sentmail/sentmail-show.php:127
#: ../subscribers/view-subscriber-page.php:61
#: ../template/template-show.php:105
msgid "No records available."
msgstr "Rejestry nie są dostępne."

#: ../configuration/configuration-show.php:142
#: ../sentmail/deliverreport-show.php:87 ../sentmail/sentmail-show.php:139
msgid " &lt;&lt; "
msgstr "Gt"

#: ../configuration/configuration-show.php:143
#: ../sentmail/deliverreport-show.php:88 ../sentmail/sentmail-show.php:140
msgid " &gt;&gt; "
msgstr "&gt;&gt;"

#: ../export/export-email-address.php:39 ../export/export-email-address.php:45
#: ../export/export-email-address.php:50 ../export/export-email-address.php:55
msgid "Unexpected url submit has been detected"
msgstr "Wykryto nieoczekiwane adresy url"

#: ../job/optin.php:65 ../job/optin.php:81 ../job/unsubscribe.php:61
#: ../job/unsubscribe.php:71
msgid ""
"Oops.. We are getting some technical error. Please try again or contact "
"admin."
msgstr ""
"Ups .. Błąd techniczny. Proszę spróbować ponownie lub skontaktować się z "
"administratorem."

#: ../job/optin.php:71
msgid "This email address has already been confirmed."
msgstr ""

#: ../sendmail/sendmail-subscriber.php:19
msgid "Please select mail configuration."
msgstr "Proszę wybrać konfigurację mail."

#: ../sendmail/sendmail-subscriber.php:26
msgid "No email address selected."
msgstr "Nie wybrano adresu e-mail."

#: ../sendmail/sendmail-subscriber.php:43
msgid "Mail sent successfully"
msgstr "Wysłano poprawnie"

#: ../sendmail/sendmail-subscriber.php:51
msgid "Click here for details"
msgstr "Kliknij, aby zobaczyć szczegóły."

#: ../sendmail/sendmail-subscriber.php:83
msgid "Send Mail Manually"
msgstr "Wyślij mail ręcznie"

#: ../sendmail/sendmail-subscriber.php:90
msgid "Select your mail configuration"
msgstr "Wybierz konfigurację poczty"

#: ../sendmail/sendmail-subscriber.php:91
msgid ""
"Select a mail configuration from available list. To create please check mail "
"configuration menu."
msgstr ""
"Wybierz konfigurację mail z listy dostępnych. Aby utworzyć sprawdź menu "
"konfiguracji poczty."

#: ../sendmail/sendmail-subscriber.php:96
msgid "Select"
msgstr "Wybierz"

#: ../sendmail/sendmail-subscriber.php:116
msgid "Mail count for one shot :"
msgstr "Liczba wiadomości w jednym strzałem:"

#: ../sendmail/sendmail-subscriber.php:128
#: ../sendmail/sendmail-subscriber.php:129
msgid "Select email address page"
msgstr "Wybierz adres e-mail strona"

#: ../sendmail/sendmail-subscriber.php:149
msgid "No Subscribers"
msgstr "Brak zapisani"

#: ../sendmail/sendmail-subscriber.php:153
msgid "Check All"
msgstr "Zaznacz wszystko"

#: ../sendmail/sendmail-subscriber.php:154
msgid "Uncheck All"
msgstr "Odznacz wszystko"

#: ../sentmail/deliverreport-show.php:9
msgid "Oops.. Unexpected error occurred. Please try again."
msgstr "Ups .. Wystąpił nieoczekiwany błąd. Proszę spróbuj ponownie."

#: ../sentmail/deliverreport-show.php:17
msgid "Mail Delivery Report"
msgstr "Raport doręczenia"

#: ../sentmail/deliverreport-show.php:35 ../sentmail/deliverreport-show.php:45
#: ../subscribers/view-subscriber-page.php:19
#: ../subscribers/view-subscriber-show.php:226
msgid "Email"
msgstr "Email"

#: ../sentmail/deliverreport-show.php:36 ../sentmail/deliverreport-show.php:46
msgid "Sent Date"
msgstr "Data wysłania"

#: ../sentmail/deliverreport-show.php:37 ../sentmail/deliverreport-show.php:47
msgid "Viewed Status"
msgstr "Wyświetl status"

#: ../sentmail/deliverreport-show.php:38 ../sentmail/deliverreport-show.php:48
msgid "Viewed Date"
msgstr "Wyświetl date"

#: ../sentmail/deliverreport-show.php:39 ../sentmail/deliverreport-show.php:49
#: ../subscribers/view-subscriber-page.php:22
#: ../subscribers/view-subscriber-page.php:32
#: ../subscribers/view-subscriber-show.php:229
#: ../subscribers/view-subscriber-show.php:241
msgid "Database ID"
msgstr "Identyfikator"

#: ../sentmail/sentmail-preview.php:19 ../template/template-preview.php:17
msgid "Preview Template"
msgstr "Podgląd szablonu"

#: ../sentmail/sentmail-show.php:43
msgid "Successfully deleted all records except latest 20."
msgstr "Pomyślnie usunięte wszystkie rekordy z wyjątkiem ostatniego 20."

#: ../sentmail/sentmail-show.php:55
msgid "Sent Mail"
msgstr "Wysłany mail"

#: ../sentmail/sentmail-show.php:73 ../sentmail/sentmail-show.php:85
msgid "View Reports"
msgstr "Zobacz raporty"

#: ../sentmail/sentmail-show.php:74 ../sentmail/sentmail-show.php:86
msgid "Mail Preview"
msgstr "Poczta Podgląd"

#: ../sentmail/sentmail-show.php:75 ../sentmail/sentmail-show.php:87
msgid "Sent Source"
msgstr "Wyślij Źródło"

#: ../sentmail/sentmail-show.php:76 ../sentmail/sentmail-show.php:88
msgid "Sent Start Date"
msgstr "Wyślij Date rozpoczęcia"

#: ../sentmail/sentmail-show.php:77 ../sentmail/sentmail-show.php:89
msgid "Sent End Date"
msgstr "Wyślij Date zakończenia"

#: ../sentmail/sentmail-show.php:78 ../sentmail/sentmail-show.php:90
msgid "Total Mails"
msgstr "Wszystkie wiadomości"

#: ../sentmail/sentmail-show.php:79 ../sentmail/sentmail-show.php:91
#: ../subscribers/view-subscriber-export.php:31
#: ../subscribers/view-subscriber-export.php:40
#: ../subscribers/view-subscriber-show.php:230
#: ../subscribers/view-subscriber-show.php:242
#: ../template/template-show.php:59 ../template/template-show.php:68
msgid "Action"
msgstr "Akcja"

#: ../sentmail/sentmail-show.php:163 ../sentmail/sentmail-show.php:165
msgid "Optimize Table"
msgstr "Optymalizacja tabeli"

#: ../sentmail/sentmail-show.php:175
msgid ""
"Note: Please click <strong>Optimize Table</strong> button to delete all "
"records except latest 20."
msgstr ""
"Uwaga: Proszę kliknąć przycisk <strong>Optimizuj Tabele,</strong> aby usunąć "
"wszystkie raporty oprócz ostatnich 10."

#: ../settings/schedule-show.php:7
msgid "Schedule Newsletter"
msgstr "Harmonogram biuletyn"

#: ../settings/schedule-show.php:8
msgid ""
"We have plan to add wp_cron feature in the next release. At present I "
"strongly recommend you to use your server CRON job (cPanel or Plesk) to send "
"your newsletters. The following link explains how to create a CRON job "
"through the cPanel or Plesk."
msgstr ""
"Mamy plan, aby dodać funkcję wp_cron w następnej wersji. Obecnie Gorąco "
"polecam do korzystania z pracy serwera CRON (cPanel lub Plesk) do wysyłania "
"newsletterów. Poniższy link wyjaśnia jak stworzyć zadanie CRON przez cPanel "
"lub Plesk."

#: ../settings/schedule-show.php:9
msgid "How to setup auto emails (cron job) in Plesk"
msgstr "Jak skonfigurować wiadomości e-mail Auto (cron) w Plesk"

#: ../settings/schedule-show.php:10
msgid "How to setup auto emails (cron job) in cPanal"
msgstr "Jak skonfigurować wiadomości e-mail Auto (cron) w cPanal"

#: ../settings/schedule-show.php:11
msgid "Hosting doesnt support cron jobs?"
msgstr "Hosting robi miejsc pracy wsparcie cron?"

#: ../settings/settings-edit.php:102
msgid "Please enter sender of notifications from name."
msgstr "Podaj nazwę nadawcy powiadomień."

#: ../settings/settings-edit.php:108
msgid "Please enter sender of notifications from email."
msgstr "Wpisz nadawcę powiadomień z poczty e-mail."

#: ../settings/settings-edit.php:148
msgid "Details was successfully updated."
msgstr "Szczegóły pomyślnie zaktualizowane."

#: ../settings/settings-edit.php:153
msgid "Oops, details not update."
msgstr "Ups, szczegóły nie zaktualizowane."

#: ../settings/settings-edit.php:201
msgid "Sender of notifications"
msgstr "Nadawca powiadomień"

#: ../settings/settings-edit.php:202
msgid ""
"Choose a FROM name and FROM email address for all notifications emails from "
"this plugin."
msgstr ""
"Wybierz z nazwy i od adresu e-mail dla wszystkich powiadomień e-maili z tej "
"wtyczki."

#: ../settings/settings-edit.php:211
msgid "Mail type"
msgstr "Typ Poczty"

#: ../settings/settings-edit.php:212
msgid ""
"Option 1 & 2 is to send mails with default Wordpress method wp_mail(). "
"Option 3 & 4 is to send mails with PHP method mail()"
msgstr ""
"Opcja 1 i 2 jest wysyłanie maili z domyślnym Wordpress metody wp_mail (). "
"Opcja 3 i 4 jest wysyłanie maili z metodą PHP mailem ()"

#: ../settings/settings-edit.php:226
msgid "Opt-in option"
msgstr "Opt-in opcji"

#: ../settings/settings-edit.php:227
msgid ""
"Double Opt In, means subscribers need to confirm their email address by an "
"activation link sent them on a activation email message. Single Opt In, "
"means subscribers do not need to confirm their email address."
msgstr ""
"Double opt W oznacza abonenci muszą potwierdzić swój adres e-mail przez link "
"aktywacyjny wysyłany je na wiadomości e-mail aktywacyjny. Jedynka Opt W "
"oznacza abonenci nie muszą potwierdzić swój adres e-mail."

#: ../settings/settings-edit.php:238
msgid "Opt-in mail subject (Confirmation mail)"
msgstr "Opt-in mail Temat (E-mail potwierdzający)"

#: ../settings/settings-edit.php:239
msgid ""
"Enter the subject for Double Opt In mail. This will send whenever subscriber "
"added email into our database."
msgstr ""
"Wprowadź temat do podwójnego wyboru w mailu. Spowoduje to wysłanie maila gdy "
"abonent dodany do naszej bazy danych."

#: ../settings/settings-edit.php:245
msgid "Opt-in mail content (Confirmation mail)"
msgstr "Opt-in zawartości poczty elektronicznej (E-mail potwierdzający)"

#: ../settings/settings-edit.php:246
msgid ""
"Enter the content for Double Opt In mail. This will send whenever subscriber "
"added email into our database."
msgstr ""
"Wprowadź treść Pokój OPT w mailu. Spowoduje to wysłanie maila gdy abonent "
"dodany do naszej bazy danych."

#: ../settings/settings-edit.php:252
msgid "Opt-in link (Confirmation link)"
msgstr "Zapisz się odnośnik (link potwierdzający)"

#: ../settings/settings-edit.php:253
msgid "Double Opt In confirmation link. You no need to change this value."
msgstr "Double opt w link potwierdzający. Ty nie musisz zmienić tę wartość."

#: ../settings/settings-edit.php:259
msgid "Text to display after email subscribed successfully"
msgstr "Tekst wyświetlany po e-mail zapisany pomyślnie"

#: ../settings/settings-edit.php:260
msgid ""
"This text will display once user clicked email confirmation link from opt-in "
"(confirmation) email content."
msgstr ""
"Tekst ten zostanie wyświetlony, gdy użytkownik kliknął e-mail z link "
"potwierdzający opt-in (potwierdzenie) zawartości e-mail."

#: ../settings/settings-edit.php:267
msgid "Subscriber welcome email"
msgstr "Abonent mile widziane e-mail"

#: ../settings/settings-edit.php:268
msgid "To send welcome mail to subscriber, This option must be set to YES."
msgstr ""
"Aby wysłać wiadomość do powitalny abonenta, ta opcja musi być ustawiona na "
"TAK."

#: ../settings/settings-edit.php:279
msgid "Welcome mail subject"
msgstr "Witamy mail Temat"

#: ../settings/settings-edit.php:280
msgid ""
"Enter the subject for subscriber welcome mail. This will send whenever email "
"subscribed (confirmed) successfully."
msgstr ""
"Wpisz temat dla abonentów powitalnym mailu. Spowoduje to wysłanie gdy "
"subskrybowanych e-mail (potwierdzenie) z powodzeniem."

#: ../settings/settings-edit.php:286
msgid "Subscriber welcome mail content"
msgstr "Abonent treść powitanie poczty"

#: ../settings/settings-edit.php:287
msgid ""
"Enter the content for subscriber welcome mail. This will send whenever email "
"subscribed (confirmed) successfully."
msgstr ""
"Wprowadź treść abonenta powitalnym mailu. Spowoduje to wysłanie gdy "
"subskrybowanych e-mail (potwierdzenie) z powodzeniem."

#: ../settings/settings-edit.php:295
msgid "Mail to admin"
msgstr "Mail do administratora"

#: ../settings/settings-edit.php:296
msgid ""
"To send admin notifications for new subscriber, This option must be set to "
"YES."
msgstr ""
"Aby wysyłać powiadomienia administratora dla nowego abonenta, ta opcja musi "
"być ustawiona na TAK."

#: ../settings/settings-edit.php:307
msgid "Admin email addresses"
msgstr "Admin adresy e-mail"

#: ../settings/settings-edit.php:308
msgid ""
"Enter the admin email addresses that should receive notifications (separate "
"by comma)."
msgstr ""
"Wpisz adresy e-mail administratora, które powinny otrzymywać powiadomienia "
"(oddzielne przecinkiem)."

#: ../settings/settings-edit.php:314
msgid "Admin mail subject"
msgstr "Administrator poczty przedmiotem"

#: ../settings/settings-edit.php:315
msgid ""
"Enter the subject for admin mail. This will send whenever new email added "
"and confirmed into our database."
msgstr ""
"Wpisz temat dla administratora poczty. Spowoduje to wysłanie gdy dodał nowy "
"e-mail i potwierdzone do naszej bazy danych."

#: ../settings/settings-edit.php:321
msgid "Admin mail content"
msgstr "Admin zawartości poczty"

#: ../settings/settings-edit.php:322
msgid ""
"Enter the mail content for admin. This will send whenever new email added "
"and confirmed into our database."
msgstr ""
"Wprowadź treść mail do administratora. Spowoduje to wysłanie gdy dodał nowy "
"e-mail i potwierdzone do naszej bazy danych."

#: ../settings/settings-edit.php:329
msgid "Unsubscribe link"
msgstr "Wyrejestrowanie Link"

#: ../settings/settings-edit.php:330
msgid "Unsubscribe link. You no need to change this value."
msgstr "Wypisz odnośnik. Ty nie musisz zmienić tę wartość."

#: ../settings/settings-edit.php:336
msgid "Unsubscribe text in mail"
msgstr "Wypisanie tekstu w elektroniczną"

#: ../settings/settings-edit.php:337
msgid ""
"Enter the text for unsubscribe link. This text is to add unsubscribe link "
"with newsletter."
msgstr ""
"Wprowadź tekst łącze rezygnacji z subskrypcji. Ten tekst jest dodać odnośnik "
"wypisania się z newslettera."

#: ../settings/settings-edit.php:343
msgid "Text to display after email unsubscribed"
msgstr "Tekst wyświetlany po e-mail wypisany"

#: ../settings/settings-edit.php:344
msgid ""
"This text will display once user clicked unsubscribed link from our "
"newsletter."
msgstr ""
"Tekst ten zostanie wyświetlony, gdy użytkownik kliknął wypisany link z "
"naszego newslettera."

#: ../settings/settings-edit.php:351
msgid "Message 1"
msgstr "Wiadomość 1"

#: ../settings/settings-edit.php:352
msgid "Default message to display if any issue on confirmation link."
msgstr ""
"Domyślna wiadomość do wyświetlania Jeśli jakikolwiek problem na link "
"potwierdzający."

#: ../settings/settings-edit.php:358
msgid "Message 2"
msgstr "Wiadomość 2"

#: ../settings/settings-edit.php:359
msgid "Default message to display if any issue on unsubscribe link."
msgstr ""
"Domyślna wiadomość do wyświetlania Jeśli jakikolwiek problem na łącze "
"rezygnacji z subskrypcji."

#: ../settings/settings-edit.php:366
msgid "Sent report subject"
msgstr "Wysłane przedmiotem raportu"

#: ../settings/settings-edit.php:367
msgid "Mail subject for sent mail report."
msgstr "Poczta przedmiotem przesłanym raporcie mail."

#: ../settings/settings-edit.php:373
msgid "Sent report content"
msgstr "Wysłane treść raportu"

#: ../settings/settings-edit.php:374
msgid "Mail content for sent mail report."
msgstr "Zawartość mail wysłany raport mail."

#: ../settings/settings-edit.php:384
msgid "Save Settings"
msgstr "Zapisz ustawienia"

#: ../subscribers/view-subscriber-add.php:25
#: ../subscribers/view-subscriber-edit.php:45
msgid "Please enter valid email."
msgstr "Wprowadź poprawny adres email."

#: ../subscribers/view-subscriber-add.php:37
msgid "Email was successfully inserted."
msgstr "E-mail został pomyślnie wstawiony."

#: ../subscribers/view-subscriber-add.php:41
msgid "Email already exist in our list."
msgstr "E-mail już istnieje w naszej liście."

#: ../subscribers/view-subscriber-add.php:46
msgid "Email is invalid."
msgstr "Nieprawidłowy adres e-mail"

#: ../subscribers/view-subscriber-add.php:86
msgid "Add email"
msgstr "Dodaj e-mail"

#: ../subscribers/view-subscriber-add.php:88
#: ../subscribers/view-subscriber-edit.php:88
msgid "Enter full name"
msgstr "Podaj imię i nazwisko"

#: ../subscribers/view-subscriber-add.php:90
#: ../subscribers/view-subscriber-edit.php:90
msgid "Enter the name for email."
msgstr "Wpisz nazwę e-mail."

#: ../subscribers/view-subscriber-add.php:92
#: ../subscribers/view-subscriber-edit.php:92
msgid "Enter email address."
msgstr "Wpisz swój Adres E-mail"

#: ../subscribers/view-subscriber-add.php:94
#: ../subscribers/view-subscriber-edit.php:94
msgid "Enter the email address to add in the subscribers list."
msgstr "Wpisz adres e-mail, aby dodać do listy subskrybentów."

#: ../subscribers/view-subscriber-add.php:96
#: ../subscribers/view-subscriber-edit.php:96
#: ../subscribers/view-subscriber-page.php:21
#: ../subscribers/view-subscriber-page.php:31
#: ../subscribers/view-subscriber-show.php:228
#: ../subscribers/view-subscriber-show.php:240
msgid "Status"
msgstr "Status"

#: ../subscribers/view-subscriber-add.php:103
#: ../subscribers/view-subscriber-edit.php:103
msgid "Unsubscribed, Unconfirmed emails not display in send mail page."
msgstr "Niepotwierdzone maile wypisany, nie wyświetli się strona wyślij mail."

#: ../subscribers/view-subscriber-edit.php:57
msgid "Email was successfully updated."
msgstr "E-mail został pomyślnie zaktualizowany."

#: ../subscribers/view-subscriber-edit.php:86
msgid "Edit email"
msgstr "Zmień e-mail"

#: ../subscribers/view-subscriber-export.php:22
msgid "Export email address in csv format"
msgstr "Adres e-mail w formacie csv eksport"

#: ../subscribers/view-subscriber-export.php:28
#: ../subscribers/view-subscriber-export.php:37
#: ../subscribers/view-subscriber-page.php:18
#: ../subscribers/view-subscriber-page.php:28
#: ../subscribers/view-subscriber-show.php:225
#: ../subscribers/view-subscriber-show.php:237
msgid "Sno"
msgstr "Lp"

#: ../subscribers/view-subscriber-export.php:29
#: ../subscribers/view-subscriber-export.php:38
msgid "Export option"
msgstr "Opcje eksportu"

#: ../subscribers/view-subscriber-export.php:30
#: ../subscribers/view-subscriber-export.php:39
msgid "Total email"
msgstr "Razem email"

#: ../subscribers/view-subscriber-export.php:47
msgid "Subscriber email address"
msgstr "Abonent adres e-mail"

#: ../subscribers/view-subscriber-export.php:49
#: ../subscribers/view-subscriber-export.php:56
#: ../subscribers/view-subscriber-export.php:63
msgid "Click to export csv"
msgstr "Kliknij na eksport CSV"

#: ../subscribers/view-subscriber-export.php:54
msgid "Registered email address"
msgstr "Zarejestrowany adres e-mail"

#: ../subscribers/view-subscriber-export.php:61
msgid "Comments author email address"
msgstr "Komentarze autor adres e-mail"

#: ../subscribers/view-subscriber-export.php:70
msgid "Add Email"
msgstr "Dodaj e-mail"

#: ../subscribers/view-subscriber-export.php:71
#: ../subscribers/view-subscriber-show.php:214
#: ../subscribers/view-subscriber-show.php:339
msgid "Import Email"
msgstr "Import e-mail"

#: ../subscribers/view-subscriber-import.php:75
msgid "Email(s) was successfully imported."
msgstr "Adres (y) E-mail został pomyślnie zaimportowany."

#: ../subscribers/view-subscriber-import.php:76
msgid "Email(s) are already in our database."
msgstr "E-mail (e) są już w naszej bazie danych."

#: ../subscribers/view-subscriber-import.php:77
msgid "Email(s) are invalid."
msgstr "Adres email(ów) jest nieprawidłowy."

#: ../subscribers/view-subscriber-import.php:87
msgid "File upload failed or no data available in the csv file."
msgstr "Przesyłanie pliku nie powiodło się lub brak danych w pliku csv."

#: ../subscribers/view-subscriber-import.php:121
msgid "Upload email"
msgstr "Prześlij e-mail"

#: ../subscribers/view-subscriber-import.php:122
msgid "Select csv file"
msgstr "Wybierz plik CSV"

#: ../subscribers/view-subscriber-import.php:124
msgid ""
"Please select the input csv file. Please check official website for csv "
"structure."
msgstr ""
"Proszę wybrać plik csv. Proszę sprawdzić Oficjalna strona struktury csv."

#: ../subscribers/view-subscriber-import.php:129
msgid "Upload CSV"
msgstr "Dodaj CSV"

#: ../subscribers/view-subscriber-page.php:13
#: ../subscribers/view-subscriber-show.php:183
msgid "View subscriber"
msgstr "Lista subskrybentów"

#: ../subscribers/view-subscriber-page.php:29
#: ../subscribers/view-subscriber-show.php:238
msgid "Email address"
msgstr "Email"

#: ../subscribers/view-subscriber-show.php:68
#: ../subscribers/view-subscriber-show.php:121
msgid ""
"To send confirmation mail, Please change the Opt-in option to Double Opt In."
msgstr "Aby wysłać maila potwierdzającego, proszę zmienić opcję opt-in."

#: ../subscribers/view-subscriber-show.php:77
msgid "Confirmation email resent successfully."
msgstr "E-mail z potwierdzeniem wysłano poprawnie."

#: ../subscribers/view-subscriber-show.php:106
#: ../subscribers/view-subscriber-show.php:152
msgid "Oops, No record was selected."
msgstr "Ups, nie wybrano rekordu."

#: ../subscribers/view-subscriber-show.php:146
msgid "Confirmation email(s) resent successfully."
msgstr "Poprawnie wysłano e-maile potwierdzające"

#: ../subscribers/view-subscriber-show.php:215
#: ../subscribers/view-subscriber-show.php:340
msgid "Export Email (CSV)"
msgstr "Eksport e-mail (CSV)"

#: ../subscribers/view-subscriber-show.php:283
#: ../subscribers/view-subscriber-show.php:318
msgid "Resend Confirmation"
msgstr "Wyślij ponownie potwierdzenie"

#: ../subscribers/view-subscriber-show.php:299
msgid ""
"No records available. Please use the above alphabet search button to search."
msgstr ""
"Rejestry niedostępne. Proszę kliknąć na powyższy przycisk wyszukiwania "
"alfabetycznego."

#: ../subscribers/view-subscriber-show.php:316
msgid "Bulk Actions"
msgstr "Masowe działania"

#: ../subscribers/view-subscriber-show.php:320
msgid "Apply"
msgstr "Zastosuj"

#: ../subscribers/view-subscriber-show.php:322
msgid "View all status"
msgstr "Zobacz wszystkie statusy"

#: ../subscribers/view-subscriber-show.php:323
msgid "Confirmed"
msgstr "Potwierdzony"

#: ../subscribers/view-subscriber-show.php:324
msgid "Unconfirmed"
msgstr "Niepotwierdzone"

#: ../subscribers/view-subscriber-show.php:325
msgid "Unsubscribed"
msgstr "Zrezygnowano z subskrypcji"

#: ../subscribers/view-subscriber-show.php:326
msgid "Single Opt In"
msgstr "Jedynka Opt W"

#: ../template/template-add.php:26 ../template/template-edit.php:40
msgid "Please enter template heading."
msgstr "Podaj nagłówek szablonu."

#: ../template/template-add.php:44
msgid "Template was successfully created."
msgstr "Szablon został utworzony."

#: ../template/template-add.php:80
msgid "Create Template"
msgstr "Utwórz szablon"

#: ../template/template-add.php:83 ../template/template-edit.php:91
msgid "Enter template heading."
msgstr "Podaj nagłówek szablonu."

#: ../template/template-add.php:85 ../template/template-edit.php:93
msgid "Please enter your email subject."
msgstr "Proszę wpisać temat wiadomości."

#: ../template/template-add.php:87 ../template/template-edit.php:95
msgid "Template header"
msgstr "Szablon nagłówka"

#: ../template/template-add.php:90 ../template/template-edit.php:98
msgid "Please create header portion for your template."
msgstr "Proszę utworzyć część nagłówka dla szablonu."

#: ../template/template-add.php:92 ../template/template-edit.php:100
msgid "Template body"
msgstr "Szablon ciała"

#: ../template/template-add.php:95 ../template/template-edit.php:103
msgid "Please create body portion for your template."
msgstr "Proszę utworzyć korpus do szablonu."

#: ../template/template-add.php:98 ../template/template-edit.php:106
msgid "Template footer"
msgstr "Szablon stopki"

#: ../template/template-add.php:101 ../template/template-edit.php:109
msgid "Please create footer portion for your template."
msgstr "Proszę utworzyć część stopki w szablonie."

#: ../template/template-edit.php:56
msgid "Template was successfully updated."
msgstr "Szablon został pomyślnie zaktualizowany."

#: ../template/template-edit.php:88
msgid "Edit Template"
msgstr "Edycja szablonu"

#: ../template/template-show.php:45
msgid "Mail Template"
msgstr "Szablon Poczta"

#: ../template/template-show.php:57
msgid "Template Heading"
msgstr "Szablon Nagłówek"

#: ../template/template-show.php:58 ../template/template-show.php:67
msgid "Type"
msgstr "Typ"

#: ../template/template-show.php:66
msgid "Email subject"
msgstr "Temat e-mail"

#: ../template/template-show.php:115
msgid "Create New Template"
msgstr "Utwórz nowy szablon"

#~ msgid " Selected record was successfully deleted."
#~ msgstr "Wybrany rekord został pomyślnie usunięty."

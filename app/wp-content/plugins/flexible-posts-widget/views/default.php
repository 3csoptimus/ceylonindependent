<?php
/**
 * Flexible Posts Widget: Default widget template
 * 
 * @since 3.4.0
 *
 * This template was added to overcome some often-requested changes
 * to the old default template (widget.php).
 */

// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');

//echo $before_widget;

if ( ! empty( $title ) )
	echo $before_title . $title . $after_title;

if ( $flexible_posts->have_posts() ):
?>
	<ul class="dpe-flexible-posts" style="margin:0px; list-style:none;">
	<?php while ( $flexible_posts->have_posts() ) : $flexible_posts->the_post(); global $post; ?>
		<li id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php  
				$post_id = get_the_ID();
				$category = get_the_category($post_id);
				$post_categories = $category[0]->cat_name;
			?>
			<div class="<?= $post_categories ?>" style="position:relative; height:30px; padding: 3px 10px 3px 10px; margin-bottom: 2px;">
				<a style="color: #FFFFFF;font-weight: bolder;" href="#" title="<?= $post_categories; ?>">
					<?= $post_categories ?>
				</a>
			</div>
			<a href="<?php echo the_permalink(); ?>">
				<?php
					if ( $thumbnail == true ) {
						// If the post has a feature image, show it
						if ( has_post_thumbnail() ) {
							the_post_thumbnail( $thumbsize );
						// Else if the post has a mime type that starts with "image/" then show the image directly.
						} elseif ( 'image/' == substr( $post->post_mime_type, 0, 6 ) ) {
							echo wp_get_attachment_image( $post->ID, $thumbsize );
						}
					}
				?>
				

				<div class="title" style="position: relative; bottom: 50px; left: 3px; color: #FFF; font-weight: bold; font-size: 12px; width: 95%; height: 10px;"><h3 style="color: white; font-size: 14px; background: rgba(0,0,0,0.90); padding: 1px 5px 1px 5px; font-weight: 300; font-family: inherit;"><?php the_title();?></h3></div>
			</a>
		</li>
	<?php endwhile; ?>
	</ul><!-- .dpe-flexible-posts -->
<?php	
endif; // End have_posts()
	
//echo $after_widget;